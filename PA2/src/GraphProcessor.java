// LEAVE THIS FILE IN THE DEFAULT PACKAGE
//  (i.e., DO NOT add 'package cs311.pa1;' or similar)

// DO NOT MODIFY THE EXISTING METHOD SIGNATURES
//  (you may, however, add additional methods and fields)

// DO NOT INCLUDE LIBRARIES OUTSIDE OF THE JAVA STANDARD LIBRARY
//  (i.e., you may include java.util.ArrayList etc. here, but not junit, apache commons, google guava, etc.)

/**
 * @author Ning Zhang
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class GraphProcessor {
	// other member fields and methods
	HashMap<String, HashSet<String>> map;
//	public String E;

	 public int E;

	// NOTE: graphData should be an absolute file path
	public GraphProcessor(String graphData) throws IOException {
		map = new HashMap<String, HashSet<String>>();
		FileReader fileReader = new FileReader(graphData);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		bufferedReader.readLine();
		String line = new String();
		while ((line = bufferedReader.readLine()) != null) {
			String[] parts = line.split(" ");
			addEdge(parts[0], parts[1]);
		}
		bufferedReader.close();
	}

	// public GraphProcessor() {
	// map = new HashMap<String, HashSet<String>>();
	// }

	// public String toString() {
	// String s = "";
	// for (String f : map.keySet()) {
	// for (String e : map.get(f)) {
	// s += f + " " + e.toString() + "\n";
	// }
	// ;
	// }
	// return s;
	// }

	public int outDegree(String v) {
		return map.containsKey(v) ? map.get(v).size() : 0;
	}

	private void addEdge(String u, String v) {
		if (!map.containsKey(u))
			map.put(u, new HashSet<String>());
		if (!map.containsKey(v))
			map.put(v, new HashSet<String>());
		if (!map.get(u).contains(v)) {
			map.get(u).add(v);
			 E++;
		}
	}

	public ArrayList<String> bfsPath(String u, String v) {
		HashSet<String> visited = new HashSet<String>();
		Queue<String> queue = new LinkedList<String>();
		ArrayList<String> path = new ArrayList<String>();
		queue.offer(u);
		visited.add(u);
		if (map.containsKey(u) && map.containsKey(v))
			while (!queue.isEmpty()) {
				String cur = queue.poll();
				path.add(cur);
				if (cur.equals(v))
					return path;
				for (String e : map.get(cur))
					if (!visited.contains(e)) {
						visited.add(e);
						queue.add(e);
					}
			}
		return new ArrayList<String>();
	}

	public int diameter() {
		HashMap<String, Integer> V = new HashMap<String, Integer>();
		int idx = 0;
		for (String e : map.keySet())
			V.put(e, idx++);
		int size = V.size();
		Integer[][] dist = new Integer[size][size];
		for (int i = 0; i < size; i++)
			dist[i][i] = 0;
		for (String u : map.keySet())
			for (String v : map.get(u))
				dist[V.get(u)][V.get(v)] = 1;

		int max = 0;
		for (int k = 0; k < size; k++)
			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++) {
					if (dist[i][k] != null && dist[k][j] != null)
						if (dist[i][j] == null || dist[i][j] > dist[i][k] + dist[k][j]) {
							dist[i][j] = dist[i][k] + dist[k][j];
							if (max < dist[i][j])
								max = dist[i][j];
						}
				}
		return max;
	}

	public int centrality(String v) {
		HashMap<String, Integer> VtoIdx = new HashMap<String, Integer>();
		HashMap<Integer, String> IdxtoV = new HashMap<Integer, String>();
		int idx = 0;
		for (String e : map.keySet()) {
			VtoIdx.put(e, idx);
			IdxtoV.put(idx++, e);
		}
		int size = VtoIdx.size();
		Integer[][] dist = new Integer[size][size];
		for (int i = 0; i < size; i++)
			dist[i][i] = 0;
		for (String u : map.keySet())
			for (String e : map.get(u))
				dist[VtoIdx.get(u)][VtoIdx.get(e)] = 1;

		int max = 0;
		for (int k = 0; k < size; k++)
			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++) {
					if (dist[i][k] != null && dist[k][j] != null)
						if (dist[i][j] == null || dist[i][j] > dist[i][k] + dist[k][j]) {
							dist[i][j] = dist[i][k] + dist[k][j];
							if (max < dist[i][j])
								max = dist[i][j];
						}
				}
		
		ArrayList<Integer> in = new ArrayList<Integer>();
		ArrayList<Integer> out = new ArrayList<Integer>();
		int index = VtoIdx.get(v);
		for (int i = 0; i < size; i++) {
			if (dist[i][index] != null && dist[i][index] != 0)
				in.add(i);
			if (dist[index][i] != null && dist[index][i] != 0)
				out.add(i);
		}
		int count = 0;
		for (Integer i:in)
			for (Integer j:out)
				if (dist[i][j] == dist[i][index] + dist[index][j])
					count++;
		return count;
	}

}