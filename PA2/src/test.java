import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 
 * @author Ning Zhang
 *
 */
public class test {

	public static void main(String args[]) throws IOException {
		
//		long start = 0;
//		start = System.currentTimeMillis();
//		ArrayList<String> topics = new ArrayList<String>();
//		WikiCrawler w = new WikiCrawler("/wiki/Complexity_Science", 200, topics, "WikiCS.txt");
//		try {
//			w.crawl();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		System.out.println((System.currentTimeMillis() - start));
//		
		GraphProcessor g = new GraphProcessor("WikiCS.txt");
		
//		GraphProcessor G = new GraphProcessor();
//		G.addEdge("A", "B");
//		G.addEdge("A", "C");
//		G.addEdge("B", "D");
//		G.addEdge("C", "D");
//		G.addEdge("D", "E");
//		G.addEdge("D", "F");
//		G.addEdge("E", "G");
//		G.addEdge("F", "G");
//		for (String e:G.bfsPath("A","G"))
//			System.out.println(e);
//		System.out.println(G.centrality("D"));
		
//		System.out.println(g.diameter());
//		System.out.println(g.centrality("/wiki/Complexity_theory"));
		 System.out.println("Graph constructed edge lines: " + g.E);
		 System.out.println("GraphProcessor edges: " + g.E);
		
		int maxOD = 0;
		String s = "";
		for (String e:g.map.keySet()) {
			int od = g.outDegree(e);
			if (maxOD < od) {
				maxOD = od;
				s = e;
			}
		}
		System.out.println("Hightest out degree: " + maxOD + " at Vertex: " + s);
		System.out.println("Diameter of the Graph is: " + g.diameter());
		
		int max = 0;
		s = "";
		for (String e:g.map.keySet()) {
			int c = g.centrality(e);
			if (max < c) {
				max = c;
				s = e;
			}
		}
		System.out.println("Hightest centrality: " + max + " at Vertex: " + s);
	}
	
}
