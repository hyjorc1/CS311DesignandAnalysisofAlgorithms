// LEAVE THIS FILE IN THE DEFAULT PACKAGE
//  (i.e., DO NOT add 'package cs311.pa1;' or similar)

// DO NOT MODIFY THE EXISTING METHOD SIGNATURES
//  (you may, however, add additional methods and fields)

// DO NOT INCLUDE LIBRARIES OUTSIDE OF THE JAVA STANDARD LIBRARY
//  (i.e., you may include java.util.ArrayList etc. here, but not junit, apache commons, google guava, etc.)

/**
 * @author Ning Zhang
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikiCrawler {
	static final String BASE_URL = "https://en.wikipedia.org";
	// other member fields and methods

	private String seedUrl;

	private int max;

	private ArrayList<String> topics;

	private String fileName;

	private int fectchRequestCount;

	public WikiCrawler(String seedUrl, int max, ArrayList<String> topics, String fileName) {
		this.seedUrl = seedUrl;
		this.max = max;
		this.topics = topics;
		this.fileName = fileName;
		this.fectchRequestCount = 0;
	}

	// NOTE: extractLinks takes the source HTML code, NOT a URL
	public static ArrayList<String> extractLinks(String doc) {
		ArrayList<String> links = new ArrayList<String>();
		Pattern p = Pattern.compile("(<p>)(.*)", Pattern.CASE_INSENSITIVE);
		Pattern href = Pattern.compile("href=\"([^\"]*)\"");
		Matcher m1 = p.matcher(doc);
		if (m1.find()) {
			Matcher m2 = href.matcher(m1.group(2));
			while (m2.find()) {
				String link = m2.group(1);
				String[] linkParts = link.split("/");
				if (linkParts.length == 3 && linkParts[1].equals("wiki") && !link.contains("#") && !link.contains(":"))
					links.add(link);
			}
		}
		return links;
	}

	public void crawl() throws IOException {
		Queue<String> linkQueue = new LinkedList<String>();
		Queue<String> edgeQueue = new LinkedList<String>();
		HashMap<String, String> visited = new HashMap<String, String>();
		linkQueue.offer(seedUrl);
		if (containTopics(seedUrl, visited)) {
			while (!linkQueue.isEmpty()) {
				String curLink = linkQueue.poll();
				HashSet<String> isUsed = new HashSet<String>();
				for (String link : extractLinks(visited.get(curLink))) {
					if (visited.size() != max) {
						if (!visited.containsKey(link)) {
							if (containTopics(link, visited)) {
								linkQueue.offer(link);
								edgeQueue.offer(curLink + " " + link);
								isUsed.add(link);
							}
						} else if (!link.equals(curLink) && !isUsed.contains(link)) {
							edgeQueue.offer(curLink + " " + link);
							isUsed.add(link);
						}
					} else if (visited.containsKey(link) && !link.equals(curLink) && !isUsed.contains(link)) {
						edgeQueue.offer(curLink + " " + link);
						isUsed.add(link);
					}
				}
			}
		} else {
			max = 0;
		}
		System.out.println(edgeQueue.size());
		if (fileName != null)
			writeFile(edgeQueue);
	}

	/**
	 * check if the link contains topics
	 * @param link
	 * @param visited
	 * @return
	 * @throws IOException
	 */
	private boolean containTopics(String link, HashMap<String, String> visited) throws IOException {
		if (topics == null || topics.size() == 0) {
			visited.put(link, fetchWebPageData(link));
			return true;
		}
		String pageDoc = fetchWebPageData(link);
		Pattern p = Pattern.compile("(<p>)(.*)", Pattern.CASE_INSENSITIVE);
		Matcher m1 = p.matcher(pageDoc);
		if (m1.find()) {
			String content = m1.group(2);
			for (String e : topics) {
				Pattern topicPattern = Pattern.compile("\\b" + e + "\\b", Pattern.CASE_INSENSITIVE);
				Matcher m2 = topicPattern.matcher(content);
				// System.out.println(m2.find());
				if (!m2.find())
					return false;
			}
		}
		visited.put(link, pageDoc);
		return true;
	}

	/**
	 * write edges to txt
	 * @param q
	 * @throws FileNotFoundException
	 */
	private void writeFile(Queue<String> q) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(fileName);
		writer.println(max);
		while (!q.isEmpty())
			writer.println(q.poll());
		writer.close();
	}

	/**
	 * fetch web page from wiki
	 * @param s
	 * @return
	 * @throws IOException
	 */
	private String fetchWebPageData(String s) throws IOException {
		if (fectchRequestCount == 50) {
			Thread.currentThread();
			try {
				Thread.sleep(3100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			fectchRequestCount = 0;
		}
		URL url = new URL(BASE_URL + s);
		InputStream is = url.openStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String thisLine = null;
		while ((thisLine = br.readLine()) != null)
			sb.append(thisLine);
		br.close();
		fectchRequestCount++;
		return sb.toString();
	}
}
