
public class test {

	public static void main(String[] args) {
		int k = 1000;
		String s = RandomString.getSaltString(5000);
		String[] arr = new String[s.length() - k + 1];
		for (int i = 0; i < arr.length; i++)
			arr[i] = s.substring(i, i + k);
		
//		MyString a = new MyString(arr[0], 10);
//		
//		System.out.println(a.toString().substring(0));
		
		
		System.out.println("U's length is 5000, k is " + k);
		
		long start = 0;
		
		WarWithRollHash roll = new WarWithRollHash(arr, k);
		start = System.nanoTime();
		roll.compute2k();
//		System.out.println(roll.T.size());
		System.out.println((System.nanoTime() - start) + ": WarWithRollHash run time");
		
		start = System.nanoTime();
		WarWithHash hash = new WarWithHash(arr, k);
		hash.compute2k();
//		System.out.println(hash.T.size());
		System.out.println((System.nanoTime() - start) + ": WarWithHash run time");
		
		start = System.nanoTime();
		WarWithBST bst = new WarWithBST(arr, k);
		bst.compute2k();
//		System.out.println(bst.T.size());
		System.out.println((System.nanoTime() - start) + ": WarWithBST run time");
		
		
		start = System.nanoTime();
		WarWithArray a = new WarWithArray(arr, k);
		a.compute2k();
//		System.out.println(a.T.size());
		System.out.println((System.nanoTime() - start) + ": WarWithArray run time");
	}

}
