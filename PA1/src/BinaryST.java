import java.util.ArrayList;

/*
 * 	Binary Search Tree
 * 
 *  @author Ning Zhang
 *  
 */

public class BinaryST {

	private Node root;
	private int distinctSize;
	private int size;

	/**
	 * 
	 * Node class
	 *
	 */
	private class Node {
		public Node left;
		public Node right;
		public Node parent;
		public String data;
		public ArrayList<Character> chars; // last char in the string
		public int frequency;
		public int L; // # of nodes in the left subtree
		public int rank; // rank of the current node

		public Node(String s) {
			data = s;
			left = null;
			right = null;
			parent = null;
			frequency = 1;
			L = 0;
			rank = 0;
		}

		/**
		 * help constructor used for WarWithBST
		 * 
		 * @param s
		 * @param lastChar
		 */
		public Node(String s, Character lastChar) {
			data = s;
			left = null;
			right = null;
			parent = null;
			frequency = 1;
			L = 0;
			rank = 0;
			chars = new ArrayList<Character>();
			chars.add(lastChar);
		}
	}

	/**
	 * BST Constructor
	 */
	public BinaryST() {
		root = null;
		size = 0;
		distinctSize = 0;
	}

	/**
	 * BST constructor with input array
	 * 
	 * @param s
	 *            input String array
	 */
	public BinaryST(String[] s) {
		for (String e : s)
			add(e);
	}

	/**
	 * help constructor used for WarWithBST. Store the prefix with k-1 length
	 * and the last char to each node.
	 * 
	 * @param s
	 * @param k
	 */
	public BinaryST(String[] s, int k) {
		for (String e : s)
			add(e.substring(0, k - 1), e.charAt(k - 1));
	}

	/**
	 * distinct size
	 * 
	 * @return distinct size
	 */
	public int distinctSize() {
		return distinctSize;
	}

	/**
	 * size
	 * 
	 * @return size
	 */
	public int size() {
		return size;
	}

	/**
	 * height
	 * 
	 * @return height
	 */
	public int height() {
		return findHeight(root);
	}

	/**
	 * Help method to find height 
	 * 
	 * @param node
	 * @return height of the node
	 */
	public int findHeight(Node node) {
		if (node == null)
			return 0;
		return (1 + Math.max(findHeight(node.left), findHeight(node.right)));
	}

	/**
	 * add node
	 * 
	 * @param s
	 * @return true if add successfully
	 */
	public boolean add(String s) {
		if (s == null)
			return false;
		if (root == null) {
			root = new Node(s);
			size++;
			distinctSize++;
			return true;
		}
		return recAdd(root, new Node(s));
	}

	/**
	 * help method for WarWithBST
	 * @param s
	 * @param lastChar
	 * @return
	 */
	public boolean add(String s, Character lastChar) {
		if (s == null)
			return false;
		if (root == null) {
			root = new Node(s, lastChar);
			size++;
			distinctSize++;
			return true;
		}
		if (s == "") {
			root.chars.add(lastChar);
			size++;
			root.frequency++;
			return true;
		}
		return recAdd(root, new Node(s, lastChar));
	}

	/**
	 * help method for add method
	 * 
	 * @param cur
	 *            current node
	 * @param node
	 *            added node
	 * @return true if add successfully
	 */
	private boolean recAdd(Node cur, Node node) {
		if (cur == null || node == null)
			throw new NullPointerException();
		int comValue = node.data.compareTo(cur.data);
		if (comValue == 0) {
			cur.frequency++;
			size++;
			if (cur.chars != null)
				cur.chars.addAll(node.chars);
			return true;
		}
		if (comValue > 0) {
			if (cur.right == null) {
				cur.right = node;
				node.parent = cur;
				size++;
				distinctSize++;
				return true;
			} else
				return recAdd(cur.right, node);
		}
		cur.L++;
		if (cur.left == null) {
			cur.left = node;
			node.parent = cur;
			size++;
			distinctSize++;
			return true;
		} else
			return recAdd(cur.left, node);
	}

	/**
	 * search for the specific node
	 * 
	 * @param s
	 * @return true if found
	 */
	public boolean search(String s) {
		return find(root, s, 0, 0) != null;
	}

	/**
	 * multi-purposes find node method
	 * 
	 * @param cur
	 * @param s
	 * @param sub
	 *            subtraction used for remove method in order to reduce # of
	 *            nodes in left subtree
	 * @param rank
	 *            tracking rank used for rankof method
	 * @return null if not found
	 */
	public Node find(Node cur, String s, int sub, int rank) {
		if (cur == null)
			return null;
		int comValue = s.compareTo(cur.data);
		if (comValue == 0) {
			cur.rank = cur.L + rank;
			return cur;
		}
		if (comValue > 0) {
			rank += (cur.L + cur.frequency);
			return find(cur.right, s, sub, rank);
		}
		cur.L -= sub;
		return find(cur.left, s, sub, rank);
	}

	/**
	 * help method for WarWithBST to return all possible chars which can fellow the prefix
	 * @param cur
	 * @param s
	 * @return
	 */
	public ArrayList<Character> findChars(Node cur, String s) {
		if (s == null || cur == null)
			return null;
		int comValue = s.compareTo(cur.data);
		if (comValue == 0) {
			return cur.chars;
		}
		if (comValue > 0) {
			return findChars(cur.right, s);
		}
		return findChars(cur.left, s);
	}
	
	public Node root() {
		return root;
	}

	/**
	 * frequency
	 * 
	 * @param s
	 * @return
	 */
	public int frequency(String s) {
		Node node = find(root, s, 0, 0);
		return node != null ? node.frequency : -1;
	}

	/**
	 * First need to decide if the node exsits in the tree then, reduce the L
	 * value in the path for those node has "left" parent
	 * 
	 * @param s
	 * @return true if remove successfully
	 */

	public boolean remove(String s) {
		if (s == null || !search(s))
			return false;
		Node cur = find(root, s, 1, 0);
		if (cur.frequency > 1) {
			cur.frequency--;
			size--;
			return true;
		}
		remove(cur);
		distinctSize--;
		size--;
		return true;
	}

	/**
	 * remove help method
	 * 
	 * @param node
	 */
	private void remove(Node node) {
		if (node == null)
			throw new NullPointerException();
		Node cur = node;
		if (cur.left != null && cur.right != null) {
			cur = cur.right;
			while (cur.left != null) {
				cur.L--;
				cur = cur.left;
			}
			node.data = cur.data;
			node.frequency = cur.frequency;
			if (cur.parent.left == cur)
				cur.parent.left = null;
			else
				cur.parent.right = null;
			return;
		}
		if (cur.left != null)
			link(cur.left, cur.parent);
		if (cur.right != null)
			link(cur.right, cur.parent);
		if (cur.left == null && cur.right == null) {
			if (cur.parent.left == cur)
				cur.parent.left = null;
			else
				cur.parent.right = null;
		}

	}

	/**
	 * link child to parent
	 * 
	 * @param child
	 * @param parent
	 */
	private void link(Node child, Node parent) {
		if (parent == null) {
			root = child;
			child.parent = null;
		} else {
			if (parent.left == child.parent) {
				parent.left = child;
			} else
				parent.right = child;
			child.parent = parent;
		}
	}

	/**
	 * inorder array of the tree
	 * 
	 * @return inorder array of the tree
	 */
	public String[] inOrder() {
		ArrayList<String> al = new ArrayList<String>();
		recInOrder(root, al);
		String[] arr = new String[al.size()];
		for (int i = 0; i < al.size(); i++)
			arr[i] = al.get(i);
		return arr;
	}

	/**
	 * help method for inorder traverse
	 * 
	 * @param cur
	 * @param al
	 */
	private void recInOrder(Node cur, ArrayList<String> al) {
		if (cur == null)
			return;
		recInOrder(cur.left, al);
		for (int i = 0; i < cur.frequency; i++)
			al.add(cur.data);
		recInOrder(cur.right, al);
	}
	
	/**
	 * help method for WarWithBST
	 * @return
	 */
	public ArrayList<String> warInOrder() {
		ArrayList<String> al = new ArrayList<String>();
		recWarInOrder(root, al);
		return al;
	}
	
	/**
	 * help method for WarWithBST
	 * @param cur
	 * @param al
	 */
	private void recWarInOrder(Node cur, ArrayList<String> al) {
		if (cur == null)
			return;
		recWarInOrder(cur.left, al);
		for (int i = 0; i < cur.frequency; i++)
			al.add(cur.data + cur.chars.get(i));
		recWarInOrder(cur.right, al);
	}

	/**
	 * preorder array of the tree
	 * 
	 * @return preorder array of the tree
	 */
	public String[] preOrder() {
		ArrayList<String> al = new ArrayList<String>();
		recPreOrder(root, al);
		String[] arr = new String[al.size()];
		for (int i = 0; i < al.size(); i++)
			arr[i] = al.get(i);
		return arr;
	}

	/**
	 * help method for preorder traverse
	 * 
	 * @param cur
	 * @param al
	 */
	private void recPreOrder(Node cur, ArrayList<String> al) {
		if (cur == null)
			return;
		for (int i = 0; i < cur.frequency; i++)
			al.add(cur.data);
		recPreOrder(cur.left, al);
		recPreOrder(cur.right, al);
	}

	/**
	 * rank of the node if it exists
	 * 
	 * @param s
	 * @return rank of the node if it exists
	 */
	public int rankOf(String s) {
		Node node = find(root, s, 0, 0);
		return node != null ? node.rank : -1;
	}

	public void printArr(String[] arr) {
		for (String e : arr)
			System.out.print(e + " ");
		System.out.println();
	}

	public void printTree() {
		if (root == null)
			return;
		recPrintTree(root, ">");
	}

	public void recPrintTree(Node r, String level) {
		if (r == null) {
			System.out.println(level + "null");
			return;
		}
		System.out.println(level + r.data);
		recPrintTree(r.left, " " + level);
		recPrintTree(r.right, " " + level);
	}

	public static void main(String[] args) {

		String[] arr = new String[] { "D", "B", "A", "C", "F" };

		BinaryST bst = new BinaryST(arr);
		// bst.add("D");
		// bst.add("B");
		// bst.add("A");
		// bst.add("C");
		// bst.add("F");
		bst.add("G");
		bst.add("E");
		// bst.add("A");
		// bst.add("A");
		// bst.add("A");
		// bst.add("D");
		// bst.add("D");
		// bst.add("G");
		// bst.add("G");
		bst.add("Z");

		System.out.println(bst.remove("Z"));
		// System.out.println(bst.remove("A"));
		// bst.remove("A");
		// bst.remove("A");
		// bst.remove("D");
		// bst.remove("D");
		// bst.remove("G");
		// bst.remove("G");
		// bst.remove("D");
		// bst.add("D");

		System.out.println("height: " + bst.height());
		System.out.println("size: " + bst.size());
		System.out.println("dissize: " + bst.distinctSize());
		System.out.println("A's freq: " + bst.frequency("A"));
		System.out.print("inorder: ");
		bst.printArr(bst.inOrder());
		System.out.print("preorder: ");
		bst.printArr(bst.preOrder());
		System.out.println("rank of A: " + bst.rankOf("A"));
		System.out.println("rank of B: " + bst.rankOf("B"));
		System.out.println("rank of C: " + bst.rankOf("C"));
		System.out.println("rank of D: " + bst.rankOf("D"));
		System.out.println("rank of E: " + bst.rankOf("E"));
		System.out.println("rank of F: " + bst.rankOf("F"));
		System.out.println("rank of G: " + bst.rankOf("G"));
		System.out.println("rank of Z: " + bst.rankOf("Z"));

		bst.printTree();
	}

}
