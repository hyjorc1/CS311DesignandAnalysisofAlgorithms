/**
 * 
 * @author Ning Zhang
 *
 */
public class MyString {

	private String s;
	private int id;
	private int k;
	private int p = 31;
	private int h = 1;
	

	public MyString(String s, int k) {
		this.s = s;
		this.k = k;
		id = hashFunc(s);
		patternInitMultiplyer();
	}

	public MyString(String s, int id, int k) {
		this.s = s;
		this.id = id;
		this.k = k;
		patternInitMultiplyer();
	}
	
	/**
	 * help method for calculating the first coefficient for the first character in the pattern String
	 */
	public void patternInitMultiplyer() {
		for (int i = 0; i < k - 2; i++)
			h = (h * p);
	}

	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		MyString tmp = (MyString) obj;
		return id == tmp.id && (s == tmp.s || (s != null && s.equals(tmp.s))
				|| (s != null && s.equals(tmp.s.substring(tmp.s.length() - k + 1))));
	}

	/**
	 * hash func for the pattern with length k - 1
	 * @param s
	 * @return
	 */
	public int hashFunc(String s) {
		int hash = 0;
		for (int i = (s.length() - k + 1); i < (s.length()); i++) {
			hash = hash * p + s.charAt(i);
		}
		return hash;
	}

	/**
	 * append a character at the end of the string. And recalculate the hashcode with the idea roll over hashing 
	 * instead of recalculating the whole string
	 * @param chr
	 * @return
	 */
	public MyString add(Character chr) {
		id = k == 1 ? 0 : (int) ((id - s.charAt((s.length() - k + 1)) * h) * p + chr);
		this.s += chr;
		return this;
	}

	public String toString() {
		return s;
	}

	public int length() {
		return s.length();
	}
}
