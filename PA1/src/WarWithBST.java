import java.util.ArrayList;

/**
 * WarWithBST
 * 
 * @author Ning Zhang
 *
 */
public class WarWithBST {

	private int k;
	private BinaryST prefixTree;
	protected ArrayList<String> T;
	
	/**
	 * Constructor
	 * @param s
	 * @param k
	 */
	public WarWithBST(String[] s, int k) {
		this.k = k;
		prefixTree = new BinaryST(s, k);
	}
	
	/**
	 * Return all possible 2k-length String
	 * 
	 * @return all possible 2k-length String
	 */
	public ArrayList<String> compute2k() {
		T = new ArrayList<String>();
		for (String e : prefixTree.warInOrder())
			recCompute2k(e);
		return T;
	}
	
	/**
	 * recursively compute all possible 2k-length String
	 * 
	 * @param s
	 */
	public void recCompute2k(String s) {
		if (s.length() == 2 * k) {
			T.add(s);
			return;
		}
		ArrayList<Character> chars = prefixTree.findChars(prefixTree.root(), s.substring(s.length() - k + 1));
		if (chars == null || chars.size() == 0)
			return;
		else
			for (char e : chars)
				recCompute2k(s + e);
	}

}
