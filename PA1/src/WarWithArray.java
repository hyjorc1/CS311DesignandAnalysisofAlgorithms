import java.util.ArrayList;

/**
 * WarWithArray
 * 
 * @author Ning Zhang
 *
 */
public class WarWithArray {

	private int k;
	private String[] arr;
	protected ArrayList<String> T;

	/**
	 * Constructor of WarWithArray
	 * 
	 * @param s
	 *            input String array
	 * @param k
	 *            String length
	 */
	public WarWithArray(String[] s, int k) {
		this.k = k;
		arr = new String[s.length];
		for (int i = 0; i < s.length; i++)
			arr[i] = s[i];
	}

	/**
	 * Return all possible 2k-length String
	 * 
	 * @return all possible 2k-length String
	 */
	public ArrayList<String> compute2k() {
		T = new ArrayList<String>();
		for (String e : arr)
			recCompute2k(e);
		return T;
	}

	/**
	 * recursively compute all possible 2k-length String
	 * 
	 * @param s
	 */
	public void recCompute2k(String s) {
		if (s.length() == 2 * k) {
			T.add(s);
			return;
		}
		ArrayList<Character> chars = checkPrefix(s);
		if (chars == null || chars.size() == 0)
			return;
		else
			for (char e : chars)
				recCompute2k(s + e);
	}

	/**
	 * check all prefix
	 * 
	 * @param s
	 * @return a list of all possible characters can be added to the string
	 */
	public ArrayList<Character> checkPrefix(String s) {
		ArrayList<Character> charAL = new ArrayList<Character>();
		for (String e : arr) {
			if (s.substring(s.length() - k + 1).equals(e.substring(0, k - 1)))
				charAL.add(e.charAt(k - 1));
		}
		return charAL;
	}

}
