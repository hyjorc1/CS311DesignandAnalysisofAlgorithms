import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

/**
 * 
 * @author Ning Zhang
 *
 */
public class WarWithRollHash {

	protected int k;
	protected Hashtable<MyString, ArrayList<Character>> prefixTable;
	protected ArrayList<String> T;

	/**
	 * Constructor of WarWithArray
	 * @param s
	 * @param k
	 */
	public WarWithRollHash(String[] s, int k) {
		this.k = k;
		prefixTable = new Hashtable<MyString, ArrayList<Character>>();
		for (String e : s) {
			MyString prefix = new MyString(e.substring(0, k - 1), e.length());
			Character lastChar = e.charAt(k - 1);
			if (!prefixTable.containsKey(prefix)) {
				ArrayList<Character> tmp = new ArrayList<Character>();
				tmp.add(lastChar);
				prefixTable.put(prefix, tmp);
			} else
				prefixTable.get(prefix).add(lastChar);
		}
	}
	
	/**
	 * Return all possible 2k-length String
	 * @return
	 */
	public ArrayList<String> compute2k() {
		T = new ArrayList<String>();
		for (Entry<MyString, ArrayList<Character>> e : prefixTable.entrySet())
			for (char ele : e.getValue())
				recCompute2k(new MyString(e.getKey().toString() + ele, k));
		return T;
	}
	
	/**
	 * recursively compute all possible 2k-length String
	 * @param s
	 */
	public void recCompute2k(MyString s) {
		if (s.length() == 2 * k) {
			T.add(s.toString());
			return;
		}
		ArrayList<Character> chars = prefixTable.get(s);
		if (chars == null || chars.size() == 0)
			return;
		else
			for (char e : chars)
				recCompute2k(new MyString(s.toString(), s.hashCode(), k).add(e));
	}
}
