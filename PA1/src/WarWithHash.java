import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * WarWithArray
 * 
 * @author Ning Zhang
 *
 */

public class WarWithHash {

	protected int k;
	protected Hashtable<String, ArrayList<Character>> prefixTable;
	protected ArrayList<String> T;

	/**
	 * Constructor of WarWithArray
	 * 
	 * @param s
	 *            input String array
	 * @param k
	 *            String length
	 */
	public WarWithHash(String[] s, int k) {
		this.k = k;
		prefixTable = new Hashtable<String, ArrayList<Character>>();
		for (String e : s) {
			String prefix = e.substring(0, k - 1);
			Character lastChar = e.charAt(k - 1);
			if (!prefixTable.containsKey(prefix)) {
				ArrayList<Character> tmp = new ArrayList<Character>();
				tmp.add(lastChar);
				prefixTable.put(prefix, tmp);
			} else
				prefixTable.get(prefix).add(lastChar);
		}
	}

	/**
	 * Return all possible 2k-length String
	 * 
	 * @return all possible 2k-length String
	 */
	public ArrayList<String> compute2k() {
		T = new ArrayList<String>();
		for (Map.Entry<String, ArrayList<Character>> e : prefixTable.entrySet())
			for (char ele : e.getValue())
				recCompute2k(e.getKey() + ele);
		return T;
	}

	/**
	 * recursively compute all possible 2k-length String
	 * 
	 * @param s
	 */
	public void recCompute2k(String s) {
		if (s.length() == 2 * k) {
			T.add(s);
			return;
		}
		ArrayList<Character> chars = prefixTable.get(s.substring(s.length() - k + 1));
		if (chars == null || chars.size() == 0)
			return;
		else
			for (char e : chars)
				recCompute2k(s + e);
	}

}
