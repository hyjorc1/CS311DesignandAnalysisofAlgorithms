package cs311.map;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import cs311.graph.IGraph;
import cs311.graph.IGraph.Edge;
import cs311.graphalgorithms.GraphAlgorithms;
import cs311.map.OSMMap.Edata;
import cs311.map.OSMMap.Vdata;

/**
 * 
 * @author Yijia Huang
 *
 */
public class PipeDream {

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		// TODO Auto-generated method stub
		OSMMap m = new OSMMap();
		m.LoadMap("AmesMap.txt");
		IGraph<Vdata, Edata> g = GraphAlgorithms.Kruscal(m.getGraph());
		double total = 0.0;
		for (Edge<Edata> e : g.getEdges())
			total += e.getEdgeData().getWeight();
		System.out.println("weightsum: "+total+" miles");
	}

}
