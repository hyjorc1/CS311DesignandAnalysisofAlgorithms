package cs311.map;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.*;

import cs311.graph.Graph;
import cs311.graph.IGraph;
import cs311.graph.IGraph.Edge;
import cs311.graph.IGraph.NoSuchEdgeException;
import cs311.graph.IGraph.NoSuchVertexException;
import cs311.graph.IGraph.Vertex;
import cs311.graphalgorithms.GraphAlgorithms;
import cs311.graphalgorithms.IWeight;

/**
 * 
 * @author Yijia Huang
 *
 */
public class OSMMap {

	/**
	 * The graph used to load map
	 */
	private static IGraph<Vdata, Edata> g;

	/**
	 * Construct a new graph
	 */
	public OSMMap() {
		reSet();
	}

	public static void main(String[] args)
			throws SAXException, IOException, ParserConfigurationException, NoSuchVertexException, NoSuchEdgeException {
		// part 2
		LoadMap("AmesMap.txt");
		System.out.println("Map: AmesMap.txt");
		System.out.println("Total Distance: " + TotalDistance() + " miles");
		// part 3
		if (args.length != 0) {
			LoadMap(args[0]);
			Scanner scan = new Scanner(args[1]);
			List<Location> llist = new ArrayList<Location>();
			while (scan.hasNextLine())
				llist.add(new Location(scan.nextDouble(), scan.nextDouble()));
			scan.close();
			List<String> slist = new ArrayList<String>();
			for (int i = 0; i < llist.size() - 1; i++)
				for (String e : StreetRoute(ShortestRoute(llist.get(i), llist.get(i + 1))))
					slist.add(e);
			int i = 0;
			while (i < slist.size() - 1) {
				if (slist.get(i).equals(slist.get(i + 1)))
					slist.remove(i);
				else
					i++;
			}
			for (String e : slist)
				System.out.println(e);
		}
		// Part 4
		if (args.length != 0) {
			LoadMap(args[0]);
			Scanner scan = new Scanner(args[1]);
			List<Location> llist = new ArrayList<Location>();
			while (scan.hasNextLine())
				llist.add(new Location(scan.nextDouble(), scan.nextDouble()));
			scan.close();
			List<String> ilist = new ArrayList<String>();
			for (int i = 0; i < ilist.size(); i++)
				ilist.add(ClosestRoad(llist.get(i)));
			List<String> tourlist = new ArrayList<String>();
			tourlist = ApproximateTSP(ilist);
			double total = 0.0;
			for (int i = 0; i < tourlist.size() - 1; i++)
				total += g.getEdgeData(tourlist.get(i), tourlist.get(i + 1)).getWeight();
			System.out.println("ApproximateTSP Distance: " + total);
		}

		// test
		// reSet();
		// g.addVertex("v1", new Vdata(40, 60));
		// g.addVertex("v2", new Vdata(40, 61));
		// g.addVertex("v3", new Vdata(41, 60));
		// g.addVertex("v4", new Vdata(41, 62));
		// g.addVertex("v5", new Vdata(40.5, 60.5));
		// g.addEdge("v1", "v2",
		// new
		// Edata(getDistance(g.getVertex("v1").getVertexData().getLatitude(),
		// g.getVertex("v1").getVertexData().getLongitude(),
		// g.getVertex("v2").getVertexData().getLatitude(),
		// g.getVertex("v2").getVertexData().getLongitude()), "e1"));
		// g.addEdge("v2", "v1",
		// new
		// Edata(getDistance(g.getVertex("v1").getVertexData().getLatitude(),
		// g.getVertex("v1").getVertexData().getLongitude(),
		// g.getVertex("v2").getVertexData().getLatitude(),
		// g.getVertex("v2").getVertexData().getLongitude()), "e2"));
		//
		// g.addEdge("v2", "v4",
		// new
		// Edata(getDistance(g.getVertex("v2").getVertexData().getLatitude(),
		// g.getVertex("v2").getVertexData().getLongitude(),
		// g.getVertex("v4").getVertexData().getLatitude(),
		// g.getVertex("v4").getVertexData().getLongitude()), "e3"));
		// g.addEdge("v4", "v2",
		// new
		// Edata(getDistance(g.getVertex("v2").getVertexData().getLatitude(),
		// g.getVertex("v2").getVertexData().getLongitude(),
		// g.getVertex("v4").getVertexData().getLatitude(),
		// g.getVertex("v4").getVertexData().getLongitude()), "e4"));
		//
		// g.addEdge("v3", "v4",
		// new
		// Edata(getDistance(g.getVertex("v3").getVertexData().getLatitude(),
		// g.getVertex("v3").getVertexData().getLongitude(),
		// g.getVertex("v4").getVertexData().getLatitude(),
		// g.getVertex("v4").getVertexData().getLongitude()), "e5"));
		// g.addEdge("v4", "v3",
		// new
		// Edata(getDistance(g.getVertex("v3").getVertexData().getLatitude(),
		// g.getVertex("v3").getVertexData().getLongitude(),
		// g.getVertex("v4").getVertexData().getLatitude(),
		// g.getVertex("v4").getVertexData().getLongitude()), "e6"));
		//
		// g.addEdge("v1", "v3",
		// new
		// Edata(getDistance(g.getVertex("v1").getVertexData().getLatitude(),
		// g.getVertex("v1").getVertexData().getLongitude(),
		// g.getVertex("v3").getVertexData().getLatitude(),
		// g.getVertex("v3").getVertexData().getLongitude()), "e7"));
		// g.addEdge("v3", "v1",
		// new
		// Edata(getDistance(g.getVertex("v1").getVertexData().getLatitude(),
		// g.getVertex("v1").getVertexData().getLongitude(),
		// g.getVertex("v3").getVertexData().getLatitude(),
		// g.getVertex("v3").getVertexData().getLongitude()), "e8"));
		//
		// g.addEdge("v1", "v5",
		// new
		// Edata(getDistance(g.getVertex("v1").getVertexData().getLatitude(),
		// g.getVertex("v1").getVertexData().getLongitude(),
		// g.getVertex("v5").getVertexData().getLatitude(),
		// g.getVertex("v5").getVertexData().getLongitude()), "e9"));
		// g.addEdge("v5", "v1",
		// new
		// Edata(getDistance(g.getVertex("v1").getVertexData().getLatitude(),
		// g.getVertex("v1").getVertexData().getLongitude(),
		// g.getVertex("v5").getVertexData().getLatitude(),
		// g.getVertex("v5").getVertexData().getLongitude()), "e10"));
		//
		// g.addEdge("v5", "v4",
		// new
		// Edata(getDistance(g.getVertex("v5").getVertexData().getLatitude(),
		// g.getVertex("v5").getVertexData().getLongitude(),
		// g.getVertex("v4").getVertexData().getLatitude(),
		// g.getVertex("v4").getVertexData().getLongitude()), "e11"));
		// g.addEdge("v4", "v5",
		// new
		// Edata(getDistance(g.getVertex("v5").getVertexData().getLatitude(),
		// g.getVertex("v5").getVertexData().getLongitude(),
		// g.getVertex("v4").getVertexData().getLatitude(),
		// g.getVertex("v4").getVertexData().getLongitude()), "e12"));
		//
		// System.out.println(g.getVertices().size());
		// System.out.println(g.getEdges().size());
		// System.out.println(ClosestRoad(new Location(40.001, 60.001)));
		// List<String> ilist = ShortestRoute(new Location(40.001, 60.001), new
		// Location(41.001, 62.001));
		// ilist = ShortestRoute(new Location(41.001, 62.001), new
		// Location(40.001, 60.001));
		// ilist = ShortestRoute(new Location(40.001, 61.001), new
		// Location(41.001, 60.001));
		// ilist = ShortestRoute(new Location(40.001, 61.001), new
		// Location(40.501, 60.501));
		// for (String e : ilist)
		// System.out.println(e);
		// for (String e : StreetRoute(ilist))
		// System.out.println(e);
	}

	/**
	 * Load the map into a graph
	 * 
	 * To avoid duplicate edge exception, I need to modify the hashcode() method
	 * of Edge<E> class by increasing those prime numbers:
	 * ------------------------------------------------------------------------
	 * int hash = 11; hash = 199 * hash + Objects.hashCode(this.vertex1); hash =
	 * 199 * hash + Objects.hashCode(this.vertex2);
	 * -------------------------------------------------------------------------
	 * 
	 * @param filename
	 *            file name
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static void LoadMap(String filename) throws SAXException, IOException, ParserConfigurationException {
		reSet();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new InputSource(filename));
		doc.normalize();
		NodeList nList = doc.getElementsByTagName("node");
		// int count = 1;
		for (int i = 0; i < nList.getLength(); i++) {
			Element e = (Element) nList.item(i);
			// System.out.println(count++);
			// System.out.println("id : " + e.getAttribute("id"));
			// System.out.println("lat : " + e.getAttribute("lat"));
			// System.out.println("lon : " + e.getAttribute("lon"));
			g.addVertex(e.getAttribute("id"),
					new Vdata(Double.parseDouble(e.getAttribute("lat")), Double.parseDouble(e.getAttribute("lon"))));
		}
		NodeList wList = doc.getElementsByTagName("way");
		// count = 1;
		for (int i = 0; i < wList.getLength(); i++) {
			NodeList tagList = ((Element) wList.item(i)).getElementsByTagName("tag");
			HashMap<String, String> tagmap = new HashMap<String, String>();
			for (int j = 0; j < tagList.getLength(); j++) {
				Element e = (Element) tagList.item(j);
				tagmap.put(e.getAttribute("k"), e.getAttribute("v"));
			}
			if (tagmap.containsKey("highway") && tagmap.containsKey("name")) {
				NodeList ndList = ((Element) wList.item(i)).getElementsByTagName("nd");
				String name = tagmap.get("name");
				if (tagmap.containsKey("oneway") && tagmap.get("oneway").equals("yes"))
					for (int j = 0; j < ndList.getLength() - 1; j++) {
						String v1 = ((Element) ndList.item(j)).getAttribute("ref");
						String v2 = ((Element) ndList.item(j + 1)).getAttribute("ref");
						double dist = getDistance(g.getVertexData(v1).getLatitude(), g.getVertexData(v1).getLongitude(),
								g.getVertexData(v2).getLatitude(), g.getVertexData(v2).getLongitude());
						// System.out.println(count++);
						// System.out.println("hashcode: "+new Edge<Integer>(v1,
						// v2, null).hashCode());
						// System.out.println("v1: "+ v1);
						// System.out.println("v2: "+ v2);
						// System.out.println("dist: "+ dist);
						// System.out.println("name: "+ name);
						g.addEdge(v1, v2, new Edata(dist, name));
					}
				else
					for (int j = 0; j < ndList.getLength() - 1; j++) {
						String v1 = ((Element) ndList.item(j)).getAttribute("ref");
						String v2 = ((Element) ndList.item(j + 1)).getAttribute("ref");
						double dist = getDistance(g.getVertexData(v1).getLatitude(), g.getVertexData(v1).getLongitude(),
								g.getVertexData(v2).getLatitude(), g.getVertexData(v2).getLongitude());
						// System.out.println(count++);
						// System.out.println("hashcode: "+new Edge<Integer>(v1,
						// v2, null).hashCode());
						// System.out.println("hashcode: "+new Edge<Integer>(v2,
						// v1, null).hashCode());
						// System.out.println("v1: "+ v1);
						// System.out.println("v2: "+ v2);
						// System.out.println("dist: "+ dist);
						// System.out.println("name: "+ name);
						g.addEdge(v1, v2, new Edata(dist, name));
						g.addEdge(v2, v1, new Edata(dist, name));
					}
			}
		}
	}

	/**
	 * reset to a new graph
	 */
	public static void reSet() {
		g = new Graph<Vdata, Edata>();
		g.setDirectedGraph();
	}

	/**
	 * Return the length summation of all edges in miles
	 * 
	 * @return the length summation of all edges in miles
	 */
	public static double TotalDistance() {
		double total = 0.0;
		for (Edge<Edata> e : g.getEdges())
			total += e.getEdgeData().getWeight();
		return total / 2.0;
	}

	/**
	 * Return the distance between two latitude-longitude points in miles. The
	 * original code is from:
	 * https://stackoverflow.com/questions/27928/calculate-distance-between-two-
	 * latitude-longitude-points-haversine-formula
	 * 
	 * @param lat1
	 *            latitude of the point one
	 * @param lon1
	 *            longitude of the point one
	 * @param lat2
	 *            latitude of the point two
	 * @param lon2
	 *            longitude of the point two
	 * @return the distance between two latitude-longitude points in miles
	 */
	public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
		double r = 6371; // radius of the earth in km
		double dLat = deg2rad(lat2 - lat1);
		double dLon = deg2rad(lon2 - lon1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return (r * c) * 0.621371; // in miles
	}

	/**
	 * Convert degrees to radians. The original code is from:
	 * https://stackoverflow.com/questions/27928/calculate-distance-between-two-
	 * latitude-longitude-points-haversine-formula
	 * 
	 * @param deg
	 *            the degree needed to convert
	 * @return radians
	 */
	public static double deg2rad(double deg) {
		return deg * (Math.PI / 180);
	}

	/**
	 * Return closet vertex in the graph
	 * 
	 * @param loc
	 *            a given location
	 * @return closet vertex in the graph
	 */
	public static String ClosestRoad(Location loc) {
		double lat1 = loc.getLatitude();
		double lon1 = loc.getLongitude();
		double min = Double.POSITIVE_INFINITY;
		String minid = null;
		for (Vertex<Vdata> e : g.getVertices()) {
			double lat2 = e.getVertexData().getLatitude();
			double lon2 = e.getVertexData().getLongitude();
			double tmp = getDistance(lat1, lon1, lat2, lon2);
			if (min > tmp) {
				min = tmp;
				minid = e.getVertexName();
			}
		}
		return minid;
	}

	/**
	 * Return a list of vertices' name which can construct the shortest path
	 * 
	 * @param fromLocation
	 *            start location
	 * @param toLocation
	 *            end location
	 * @return a list of vertices' name which can construct the shortest path
	 */
	public static List<String> ShortestRoute(Location fromLocation, Location toLocation) {
		String start = ClosestRoad(fromLocation);
		String end = ClosestRoad(toLocation);
		List<Edge<Edata>> elist = GraphAlgorithms.ShortestPath(g, start, end);
		List<String> ilist = new ArrayList<String>();
		ilist.add(elist.get(elist.size() - 1).getVertexName1());
		for (int i = elist.size() - 1; i >= 0; i--)
			ilist.add(elist.get(i).getVertexName2());
		return ilist;
	}

	/**
	 * Return a list of street name based on the shortest path without any
	 * consecutive duplicates
	 * 
	 * @param ilist
	 *            a list of vertices' id
	 * @return a list of street name based on the shortest path without any
	 *         consecutive duplicates
	 * @throws NoSuchVertexException
	 * @throws NoSuchEdgeException
	 */
	public static List<String> StreetRoute(List<String> ilist) throws NoSuchVertexException, NoSuchEdgeException {
		List<String> nlist = new ArrayList<String>();
		for (int i = 0; i < ilist.size() - 1; i++) {
			String name = g.getEdgeData(ilist.get(i), ilist.get(i + 1)).getStreetName();
			if (!nlist.contains(name))
				nlist.add(name);
		}
		return nlist;
	}

	/**
	 * Location class with latitude and longitude
	 * 
	 * @author Yijia Huang
	 *
	 */
	public static class Location {
		private double lat, lon;

		public Location(double lat, double lon) {
			this.lat = lat;
			this.lon = lon;
		}

		public double getLatitude() {
			return lat;
		}

		public double getLongitude() {
			return lon;
		}
	}

	/**
	 * Vertex data with latitude and longitude
	 * 
	 * @author Yijia Huang
	 *
	 */
	public static class Vdata {

		private double lat, lon;

		public Vdata(double givenlat, double givenlon) {
			lat = givenlat;
			lon = givenlon;
		}

		public double getLatitude() {
			return lat;
		}

		public double getLongitude() {
			return lon;
		}
	}

	/**
	 * Edge data with weight and street name
	 * 
	 * @author Yijia Huang
	 *
	 */
	public static class Edata implements IWeight {

		private double weight;
		private String name;

		Edata(double i, String givenname) {
			weight = i;
			name = givenname;
		}

		@Override
		public double getWeight() {
			return weight;
		}

		public String getStreetName() {
			return name;
		}
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public static List<String> ApproximateTSP(List<String> list) {
		return null;
	}

	/**
	 * Return the graph
	 * 
	 * @return the graph
	 */
	public IGraph<Vdata, Edata> getGraph() {
		return g;
	}
}
