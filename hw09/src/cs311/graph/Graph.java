
package cs311.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * 
 * @author Yijia Huang
 *
 * @param <V>
 * @param <E>
 */
public class Graph<V, E> implements IGraph<V, E> {

	/**
	 * Vertices map: key = vertices' name, value = vertices
	 */
	public HashMap<String, Vertex<V>> vmap;

	/**
	 * Neighbors map: key = vertices' name, value = set of neighboring vertices'
	 * name
	 */
	public HashMap<String, HashSet<String>> nmap;

	/**
	 * Edges map: key = edges' hashcode, value = the corresponding edge
	 */
	public HashMap<Integer, Edge<E>> emap;

	/**
	 * Check if the graph is directed
	 */
	private boolean isDirectedGraph;

	/**
	 * Constructor of graph class
	 */
	public Graph() {
		vmap = new HashMap<String, Vertex<V>>();
		nmap = new HashMap<String, HashSet<String>>();
		emap = new HashMap<Integer, Edge<E>>();
		isDirectedGraph = false;
	}

	@Override
	public void setDirectedGraph() {
		// TODO Auto-generated method stub
		isDirectedGraph = true;
	}

	@Override
	public void setUndirectedGraph() {
		// TODO Auto-generated method stub
		isDirectedGraph = false;
	}

	@Override
	public boolean isDirectedGraph() {
		// TODO Auto-generated method stub
		return isDirectedGraph;
	}

	@Override
	public void addVertex(String vertexName) throws DuplicateVertexException {
		// TODO Auto-generated method stub
		if (hasVertex(vertexName))
			throw new DuplicateVertexException();
		vmap.put(vertexName, new Vertex<V>(vertexName, null));
		nmap.put(vertexName, new HashSet<String>());
	}

	@Override
	public void addVertex(String vertexName, V vertexData) throws DuplicateVertexException {
		// TODO Auto-generated method stub
		if (hasVertex(vertexName))
			throw new DuplicateVertexException();
		vmap.put(vertexName, new Vertex<V>(vertexName, vertexData));
		nmap.put(vertexName, new HashSet<String>());
	}

	@Override
	public void addEdge(String vertex1, String vertex2) throws DuplicateEdgeException, NoSuchVertexException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertex1) || !hasVertex(vertex2))
			throw new NoSuchVertexException();
		if (hasEdge(vertex1, vertex2))
			throw new DuplicateEdgeException();
		nmap.get(vertex1).add(vertex2);
		if (!isDirectedGraph)
			nmap.get(vertex2).add(vertex1);
		emap.put(new Edge<E>(vertex1, vertex2, null).hashCode(), new Edge<E>(vertex1, vertex2, null));
	}

	@Override
	public void addEdge(String vertex1, String vertex2, E edgeData)
			throws DuplicateEdgeException, NoSuchVertexException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertex1) || !hasVertex(vertex2))
			throw new NoSuchVertexException();
		if (hasEdge(vertex1, vertex2))
			throw new DuplicateEdgeException();
		nmap.get(vertex1).add(vertex2);
		if (!isDirectedGraph)
			nmap.get(vertex2).add(vertex1);
		emap.put(new Edge<E>(vertex1, vertex2, null).hashCode(), new Edge<E>(vertex1, vertex2, edgeData));
	}

	@Override
	public V getVertexData(String vertexName) throws NoSuchVertexException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertexName))
			throw new NoSuchVertexException();
		return vmap.get(vertexName).getVertexData();
	}

	@Override
	public void setVertexData(String vertexName, V vertexData) throws NoSuchVertexException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertexName))
			throw new NoSuchVertexException();
		vmap.replace(vertexName, new Vertex<V>(vertexName, vertexData));
	}

	@Override
	public E getEdgeData(String vertex1, String vertex2) throws NoSuchVertexException, NoSuchEdgeException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertex1) || !hasVertex(vertex2))
			throw new NoSuchVertexException();
		if (!hasEdge(vertex1, vertex2))
			throw new NoSuchEdgeException();
		return getEdge(vertex1, vertex2).getEdgeData();
	}

	@Override
	public void setEdgeData(String vertex1, String vertex2, E edgeData)
			throws NoSuchVertexException, NoSuchEdgeException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertex1) || !hasVertex(vertex2))
			throw new NoSuchVertexException();
		if (!hasEdge(vertex1, vertex2))
			throw new NoSuchEdgeException();
		int c12 = new Edge<E>(vertex1, vertex2, null).hashCode();
		int c21 = new Edge<E>(vertex2, vertex1, null).hashCode();
		if (isDirectedGraph)
			emap.replace(c12, new Edge<E>(vertex1, vertex2, edgeData));
		if (emap.containsKey(c12))
			emap.replace(c12, new Edge<E>(vertex1, vertex2, edgeData));
		else
			emap.replace(c21, new Edge<E>(vertex2, vertex1, edgeData));
	}

	@Override
	public Vertex<V> getVertex(String VertexName) {
		// TODO Auto-generated method stub
		if (!hasVertex(VertexName))
			throw new NoSuchVertexException();
		return vmap.get(VertexName);
	}

	@Override
	public Edge<E> getEdge(String vertexName1, String vertexName2) {
		// TODO Auto-generated method stub
		if (!hasVertex(vertexName1) || !hasVertex(vertexName2))
			throw new NoSuchVertexException();
		if (!hasEdge(vertexName1, vertexName2))
			try {
				throw new NoSuchEdgeException();
			} catch (cs311.graph.IGraph.NoSuchEdgeException e) {
			}
		if (emap.containsKey(new Edge<E>(vertexName1, vertexName2, null).hashCode()))
			return emap.get(new Edge<E>(vertexName1, vertexName2, null).hashCode());
		return emap.get(new Edge<E>(vertexName2, vertexName1, null).hashCode());
	}

	@Override
	public List<Vertex<V>> getVertices() {
		// TODO Auto-generated method stub
		List<Vertex<V>> vlist = new ArrayList<Vertex<V>>();
		for (String e : vmap.keySet())
			vlist.add(vmap.get(e));
		return vlist;
	}

	@Override
	public List<Edge<E>> getEdges() {
		// TODO Auto-generated method stub
		List<Edge<E>> elist = new ArrayList<Edge<E>>();
		for (Integer e : emap.keySet())
			elist.add(emap.get(e));
		return elist;
	}

	@Override
	public List<Vertex<V>> getNeighbors(String vertex) {
		// TODO Auto-generated method stub
		List<Vertex<V>> nlist = new ArrayList<Vertex<V>>();
		for (String e : nmap.get(vertex))
			nlist.add(vmap.get(e));
		return nlist;
	}

	/**
	 * Help method used for checking if the vertices map contains the
	 * corresponding vertex
	 * 
	 * @param s
	 *            vertex's name
	 * @return true if the vertices map contains the corresponding vertex
	 */
	public boolean hasVertex(String s) {
		return vmap.containsKey(s);
	}

	/**
	 * Help method used for checking if the edge exists in the edges map
	 * 
	 * @param s1
	 *            vertex's name
	 * @param s2
	 *            vertex's name
	 * @return true if the edges map contains the corresponding edge
	 */
	public boolean hasEdge(String s1, String s2) {
		if (emap.containsKey(new Edge<E>(s1, s2, null).hashCode()))
			return true;
		if (!isDirectedGraph && emap.containsKey(new Edge<E>(s2, s1, null).hashCode()))
			return true;
		return false;
	}
}
