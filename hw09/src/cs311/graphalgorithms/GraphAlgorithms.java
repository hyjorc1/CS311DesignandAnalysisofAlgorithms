
package cs311.graphalgorithms;

import cs311.graph.Graph;
import cs311.graph.IGraph;
import cs311.graph.IGraph.Edge;
import cs311.graph.IGraph.Vertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * 
 * @author Yijia Huang
 *
 */
public class GraphAlgorithms {

	/**
	 * Give one topological order of vertices based on the graph g
	 * 
	 * @param g
	 *            The given graph
	 * @return a list of vertices in topological order
	 */
	public static <V, E> List<Vertex<V>> TopologicalSort(IGraph<V, E> g) {
		HashMap<String, String> statemap = new HashMap<String, String>();
		for (Vertex<V> e : g.getVertices())
			statemap.put(e.getVertexName(), "Undiscovered");
		LinkedStack<Vertex<V>> stack = new LinkedStack<Vertex<V>>();
		for (Vertex<V> e : g.getVertices())
			if (statemap.get(e.getVertexName()).equals("Undiscovered"))
				DFS(e, g, statemap, stack);
		List<Vertex<V>> vlist = new ArrayList<Vertex<V>>();
		while (!stack.isEmpty())
			vlist.add(stack.pop());
		return vlist;
	}

	/**
	 * Return all possible topological order based on the graph g
	 * 
	 * @param g
	 *            The given graph
	 * @return all possible topological order based on the graph g
	 */
	public static <V, E> List<List<Vertex<V>>> AllTopologicalSort(IGraph<V, E> g) {
		List<List<Vertex<V>>> vlistlist = new ArrayList<List<Vertex<V>>>();
		List<Vertex<V>> vlist = new ArrayList<Vertex<V>>();
		HashMap<String, String> statemap = new HashMap<String, String>();
		HashMap<String, Integer> indegreemap = new HashMap<String, Integer>();
		for (Vertex<V> e : g.getVertices()) {
			statemap.put(e.getVertexName(), "Undiscovered");
			for (Vertex<V> ele : g.getNeighbors(e.getVertexName()))
				if (!indegreemap.containsKey(ele.getVertexName()))
					indegreemap.put(ele.getVertexName(), 1);
				else
					indegreemap.replace(ele.getVertexName(), indegreemap.get(ele.getVertexName()) + 1);
		}
		AllTopologicalSortRec(g, vlistlist, vlist, statemap, indegreemap);
		return vlistlist;
	}

	/**
	 * Return a new graph with the minimum path
	 * 
	 * @param g
	 *            The given graph
	 * @return a new graph with the minimum path
	 */
	public static <V, E extends IWeight> IGraph<V, E> Kruscal(IGraph<V, E> g) {
		Heap<E> pq = new Heap<E>();
		for (Edge<E> e : g.getEdges())
			pq.add(e);
		List<List<String>> complist = new ArrayList<List<String>>();
		IGraph<V, E> G = new Graph<V, E>();
		for (Vertex<V> e : g.getVertices())
			G.addVertex(e.getVertexName(), e.getVertexData());
		// int count = 1;
		while (!pq.isEmpty()) {
			Edge<E> tmp = pq.removeMin();
			String s1 = tmp.getVertexName1();
			String s2 = tmp.getVertexName2();
			Integer c1 = Component(s1, complist);
			Integer c2 = Component(s2, complist);
			if ((c1 == null && c2 == null) || (c1 == null && c2 != null) || (c1 != null && c2 == null)
					|| ((int) c1) != ((int) c2)) {
				// System.out.println(count++);
				// System.out.println("id: " + s1 + " " + s2);
				// System.out.println("weight: " +
				// tmp.getEdgeData().getWeight());
				// System.out.println("hashcode: " + new Edge<Integer>(s1, s2,
				// null).hashCode());
				G.addEdge(s1, s2, tmp.getEdgeData());
				MergeComp(s1, s2, complist);
			}
		}
		return G;
	}

	/**
	 * Return a list of edges that the shortest path contains. Dijkstra's
	 * Algorithm
	 * 
	 * @param g
	 *            The given graph
	 * @param vertexStart
	 *            The starting vertex's name
	 * @param vertexEnd
	 *            The ending vertex's name
	 * @return a list of edges that the shortest path contains
	 */
	public static <V, E extends IWeight> List<Edge<E>> ShortestPath(IGraph<V, E> g, String vertexStart,
			String vertexEnd) {
		DistHeap<Vpair<Vertex<V>, Double>> pq = new DistHeap<Vpair<Vertex<V>, Double>>();
		List<String> closed = new ArrayList<String>();
		HashMap<String, Double> dist = new HashMap<String, Double>();
		HashMap<String, String> pred = new HashMap<String, String>();
		List<Edge<E>> edges = new ArrayList<Edge<E>>();
		for (Vertex<V> e : g.getVertices()) {
			dist.put(e.getVertexName(), Double.POSITIVE_INFINITY);
			pred.put(e.getVertexName(), null);
		}
		dist.put(vertexStart, 0.0);
		pq.add(new Vpair<Vertex<V>, Double>(g.getVertex(vertexStart), 0.0));
		while (!pq.isEmpty() && !closed.contains(vertexEnd)) {
			Vpair<Vertex<V>, Double> pair = pq.removeMin();
			Vertex<V> u = pair.getVertex();
			if (!closed.contains(u)) {
				closed.add(u.getVertexName());
				for (Vertex<V> e : g.getNeighbors(u.getVertexName())) {
					Double alt = dist.get(u.getVertexName())
							+ g.getEdge(u.getVertexName(), e.getVertexName()).getEdgeData().getWeight();
					Double vdist = dist.get(e.getVertexName());
					if (vdist == null || alt < vdist) {
						dist.put(e.getVertexName(), alt);
						pred.put(e.getVertexName(), u.getVertexName());
						pq.add(new Vpair<Vertex<V>, Double>(g.getVertex(e.getVertexName()), alt));
					}
				}
			}

		}
		if (!closed.contains(vertexEnd))
			return null;
		String tmp = vertexEnd;
		while (!tmp.equals(vertexStart)) {
			edges.add(g.getEdge(pred.get(tmp), tmp));
			tmp = pred.get(tmp);
		}
		return edges;
	}

	/**
	 * Return the index of the corresponding component in the list
	 * 
	 * @param s
	 *            The vertex's name
	 * @param complist
	 *            The component list
	 * @return the index of the corresponding component in the list
	 */
	public static Integer Component(String s, List<List<String>> complist) {
		for (int i = 0; i < complist.size(); i++)
			if (complist.get(i).contains(s))
				return i;
		return null;
	}

	/**
	 * Merge two components to one
	 * 
	 * @param s1
	 *            The vertex's name
	 * @param s2
	 *            The vertex's name
	 * @param complist
	 *            The component list
	 */
	public static void MergeComp(String s1, String s2, List<List<String>> complist) {
		Integer i1 = Component(s1, complist);
		Integer i2 = Component(s2, complist);
		if (i1 != null && i2 != null) {
			complist.get(i1).addAll(complist.get(i2));
			complist.remove((int) i2);
		} else if (i1 == null && i2 != null)
			complist.get(i2).add(s1);
		else if (i1 != null && i2 == null)
			complist.get(i1).add(s2);
		else {
			List<String> tmp = new ArrayList<String>();
			tmp.add(s1);
			tmp.add(s2);
			complist.add(tmp);
		}
	}

	/**
	 * Recursive function(backtracking method) used for finding all possible
	 * topological order based on the graph g
	 * 
	 * @param g
	 *            The given graph
	 * @param vlistlist
	 *            a list of vertex list
	 * @param vlist
	 *            vertex list
	 * @param statemap
	 *            a state map: key = vertex's name, value = discovered or
	 *            undiscovered status
	 * @param indegreemap
	 *            a indegree map: key = vertex's name, value = the indegree of
	 *            the corresponding vertex
	 */
	public static <V, E> void AllTopologicalSortRec(IGraph<V, E> g, List<List<Vertex<V>>> vlistlist,
			List<Vertex<V>> vlist, HashMap<String, String> statemap, HashMap<String, Integer> indegreemap) {
		for (Vertex<V> e : g.getVertices()) {
			if ((indegreemap.get(e.getVertexName()) == null || indegreemap.get(e.getVertexName()) == 0)
					&& statemap.get(e.getVertexName()).equals("Undiscovered")) {
				statemap.replace(e.getVertexName(), "discovered");
				vlist.add(e);
				if (vlist.size() == g.getVertices().size()) {
					List<Vertex<V>> list = new ArrayList<Vertex<V>>();
					for (Vertex<V> ele : vlist)
						list.add(ele);
					vlistlist.add(list);
				}
				for (Vertex<V> ele : g.getNeighbors(e.getVertexName()))
					indegreemap.replace(ele.getVertexName(), indegreemap.get(ele.getVertexName()) - 1);
				AllTopologicalSortRec(g, vlistlist, vlist, statemap, indegreemap);
				statemap.replace(e.getVertexName(), "Undiscovered");
				vlist.remove(vlist.size() - 1);
				for (Vertex<V> ele : g.getNeighbors(e.getVertexName()))
					indegreemap.replace(ele.getVertexName(), indegreemap.get(ele.getVertexName()) + 1);
			}
		}
	}

	/**
	 * DFS
	 * 
	 * @param v
	 *            The given vertex
	 * @param g
	 *            The given graph
	 * @param statemap
	 *            a state map: key = vertex's name, value = discovered or
	 *            undiscovered status
	 * @param stack
	 *            used for store and output vertices
	 */
	public static <V, E> void DFS(Vertex<V> v, IGraph<V, E> g, HashMap<String, String> statemap,
			LinkedStack<Vertex<V>> stack) {
		statemap.replace(v.getVertexName(), "discovered");
		for (Vertex<V> e : g.getNeighbors(v.getVertexName()))
			if (statemap.get(e.getVertexName()).equals("Undiscovered"))
				DFS(e, g, statemap, stack);
		stack.push(v);
	}

	/**
	 * stack constructed by linked list
	 * 
	 * @author Yijia Huang
	 *
	 * @param <E>
	 */
	static class LinkedStack<E> {
		private class Node {
			public E data;
			public Node link;
		}

		private Node top;
		private int numItems;

		public LinkedStack() {
			top = null;
			numItems = 0;
		}

		public int size() {
			return numItems;
		}

		public boolean isEmpty() {
			return numItems == 0;
		}

		public void push(E element) {
			Node toAdd = new Node();
			toAdd.data = element;
			toAdd.link = top;
			top = toAdd;
			numItems++;
		}

		public E pop() {
			if (top == null)
				throw new NoSuchElementException();
			E data = top.data;
			top = top.link;
			if (numItems <= 0)
				throw new RuntimeException();
			numItems--;
			return data;
		}

		public E peek() {
			if (top == null)
				throw new NoSuchElementException();
			return top.data;
		}
	}

	/**
	 * Priority queue with Edge<E> type of elements
	 * 
	 * @author Yijia Huang
	 *
	 * @param <E>
	 */
	static class Heap<E extends IWeight> {
		private static final int INIT_CAP = 10;
		private ArrayList<Edge<E>> list;

		public Heap() {
			list = new ArrayList<Edge<E>>(INIT_CAP);
		}

		public int size() {
			return list.size();
		}

		public boolean isEmpty() {
			return list.isEmpty();
		}

		public String toString() {
			return list.toString();
		}

		public void add(Edge<E> e) {
			if (e == null)
				throw new NullPointerException("add");
			list.add(e);
			percolateUp();
		}

		// move the last element up to the proper place.
		private void percolateUp() {
			int child = list.size() - 1;
			int parent;
			while (child > 0) {
				parent = (child - 1) / 2;
				if (list.get(child).getEdgeData().getWeight() - list.get(parent).getEdgeData().getWeight() >= 0)
					break;
				swap(parent, child);
				child = parent;
			}
		}

		private void swap(int parent, int child) {
			Edge<E> tmp = list.get(parent);
			list.set(parent, list.get(child));
			list.set(child, tmp);
		}

		public Edge<E> getMin() {
			if (list.isEmpty())
				throw new NoSuchElementException();
			return list.get(0);
		}

		public Edge<E> removeMin() {
			if (list.isEmpty())
				throw new NoSuchElementException();
			Edge<E> min = list.get(0);
			list.set(0, list.get(list.size() - 1));
			list.remove(list.size() - 1);
			if (!list.isEmpty())
				percolateDown(0);
			return min;
		}

		// Move the element at index start down to the proper place.
		private void percolateDown(int start) {
			if (start < 0 || start >= list.size())
				throw new RuntimeException("start < 0 or >= n");
			int parent = start;
			int child = 2 * parent + 1;
			while (child < list.size()) {
				if (child + 1 < list.size() && list.get(child).getEdgeData().getWeight()
						- list.get(child + 1).getEdgeData().getWeight() > 0)
					child++;
				if (list.get(child).getEdgeData().getWeight() - list.get(parent).getEdgeData().getWeight() >= 0)
					break;
				swap(parent, child);
				parent = child;
				child = 2 * parent + 1;
			}
		}
	}

	/**
	 * Priority queue
	 * 
	 * @author Yijia Huang
	 *
	 * @param <E>
	 */
	public static class DistHeap<E extends Comparable<? super E>> {
		private static final int INIT_CAP = 10;
		private ArrayList<E> list;

		public DistHeap() {
			list = new ArrayList<E>(INIT_CAP);
		}

		public DistHeap(int aSize) {
			if (aSize < 1)
				throw new IllegalStateException();
			list = new ArrayList<E>(aSize);
		}

		public DistHeap(List<E> aList) {
			int j, k;
			int len = aList.size();
			if (len < 1)
				throw new IllegalArgumentException();
			list = new ArrayList<E>(len);
			for (E t : aList)
				list.add(t);
			if (len < 2)
				return;
			j = (len - 2) / 2;
			for (k = j; k >= 0; k--)
				percolateDown(k);
		}

		public int size() {
			return list.size();
		}

		public boolean isEmpty() {
			return list.isEmpty();
		}

		public void add(E element) {
			if (element == null)
				throw new NullPointerException("add");
			list.add(element);
			percolateUp();
		}

		// move the last element up to the proper place.
		private void percolateUp() {
			int child = list.size() - 1;
			int parent;
			while (child > 0) {
				parent = (child - 1) / 2;
				if (list.get(child).compareTo(list.get(parent)) >= 0)
					break;
				swap(parent, child);
				child = parent;
			}
		}

		private void swap(int parent, int child) {
			E tmp = list.get(parent);
			list.set(parent, list.get(child));
			list.set(child, tmp);
		}

		public E getMin() {
			if (list.isEmpty())
				throw new NoSuchElementException();
			return list.get(0);
		}

		public E removeMin() {
			if (list.isEmpty())
				throw new NoSuchElementException();
			E minElem = list.get(0);
			list.set(0, list.get(list.size() - 1));
			list.remove(list.size() - 1);
			if (!list.isEmpty())
				percolateDown(0);
			return minElem;
		}

		// Move the element at index start down to the proper place.
		private void percolateDown(int start) {
			if (start < 0 || start >= list.size())
				throw new RuntimeException("start < 0 or >= n");
			int parent = start;
			int child = 2 * parent + 1;
			while (child < list.size()) {
				if (child + 1 < list.size() && list.get(child).compareTo(list.get(child + 1)) > 0)
					child++;
				if (list.get(child).compareTo(list.get(parent)) >= 0)
					break;
				swap(parent, child);
				parent = child;
				child = 2 * parent + 1;
			}
		}
	}

	/**
	 * A pair class with two components of types V and C, where V is a vertex
	 * data type and C is a cost type.
	 * 
	 * @author Yijia Huang
	 *
	 * @param <V>
	 * @param <C>
	 */
	private static class Vpair<V, C extends Comparable<? super C>> implements Comparable<Vpair<V, C>> {
		private V node;
		private C cost;

		Vpair(V n, C c) {
			node = n;
			cost = c;
		}

		public V getVertex() {
			return node;
		}

		public C getCost() {
			return cost;
		}

		public int compareTo(Vpair<V, C> other) {
			return cost.compareTo(other.getCost());
		}

		public String toString() {
			return "<" + node.toString() + ", " + cost.toString() + ">";
		}

		public int hashCode() {
			return node.hashCode();
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if ((obj == null) || (obj.getClass() != this.getClass()))
				return false;
			Vpair<?, ?> test = (Vpair<?, ?>) obj;
			return (node == test.node || (node != null && node.equals(test.node)));
		}
	}
}
