package cs311.graphalgorithms;

import java.util.ArrayList;
import java.util.List;

import cs311.graph.Graph;
import cs311.graph.IGraph;
import cs311.graph.IGraph.Edge;
import cs311.graph.IGraph.Vertex;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IGraph<String, Integer> g = new Graph<String, Integer>();
		g.setDirectedGraph();
		g.addVertex("v0");
		g.addVertex("v1");
		g.addVertex("v2");
		g.addVertex("v3");
		g.addVertex("v4");
		g.addVertex("v5");
		g.addEdge("v5", "v0");
		g.addEdge("v5", "v2");
		g.addEdge("v2", "v3");
		g.addEdge("v3", "v1");
		g.addEdge("v4", "v0");
		g.addEdge("v4", "v1");
		List<Vertex<String>> vlist = new ArrayList<Vertex<String>>();
		vlist = GraphAlgorithms.TopologicalSort(g);
		System.out.println("TOPOLOGICALSORT");
		for (Vertex<String> e : vlist)
			System.out.print(e.getVertexName() + " ");
		System.out.println();
		System.out.println();
		System.out.println("ALL TOPOLOGICALSORT");
		List<List<Vertex<String>>> vlistlist = GraphAlgorithms.AllTopologicalSort(g);
		for (List<Vertex<String>> e : vlistlist) {
			for (Vertex<String> ele : e)
				System.out.print(ele.getVertexName() + " ");
			System.out.println();
		}
		
		System.out.println();
		System.out.println("KruscalSORT");
		IGraph<String, data> G = new Graph<String, data>();
		G.setDirectedGraph();
		G.addVertex("A");
		G.addVertex("B");
		G.addVertex("C");
		G.addVertex("D");
		G.addVertex("E");
		G.addVertex("F");
		G.addVertex("G");
		G.addVertex("H");
		G.addEdge("A", "B", new data(1));
		G.addEdge("A", "D", new data(4));
		G.addEdge("A", "C", new data(3));
		G.addEdge("B", "E", new data(2));
		G.addEdge("C", "E", new data(5));
		G.addEdge("D", "E", new data(3));
		G.addEdge("E", "F", new data(2));
		G.addEdge("E", "H", new data(4));
		G.addEdge("D", "F", new data(2));
		G.addEdge("F", "H", new data(2));
		G.addEdge("H", "F", new data(2));
		G.addEdge("F", "G", new data(18));
		G.addEdge("H", "G", new data(1));
		G.addEdge("C", "G", new data(1));
		System.out.println(G.getEdges().size());
		IGraph<String, data> G1 = GraphAlgorithms.Kruscal(G);
//		Heap<data> pq = new Heap<data>();
//		for (Edge<data> e : G.getEdges())
//			pq.add(e);
//		System.out.println(pq.removeMin().getEdgeData().getWeight());
//		for(Vertex<String> e:G1.getVertices())
//			System.out.println(e.getVertexName());
		for(Edge<data> e:G1.getEdges())
			System.out.println(e.getVertexName1()+" "+e.getVertexName2()+" "+ e.getEdgeData().getWeight());
		
		System.out.println();
		System.out.println("ShortestPath");
		List<Edge<data>> edges = new ArrayList<Edge<data>>();
		System.out.println("Graph 1, A to G");
		edges=GraphAlgorithms.ShortestPath(G, "A", "G");
		for(Edge<data> e:edges)
			System.out.println(e.getVertexName1()+" "+e.getVertexName2()+" "+ e.getEdgeData().getWeight());
		System.out.println("Graph 1, A to F");
		edges=GraphAlgorithms.ShortestPath(G, "A", "F");
		for(Edge<data> e:edges)
			System.out.println(e.getVertexName1()+" "+e.getVertexName2()+" "+ e.getEdgeData().getWeight());
		System.out.println("Graph 1, A to H");
		edges=GraphAlgorithms.ShortestPath(G, "A", "H");
		for(Edge<data> e:edges)
			System.out.println(e.getVertexName1()+" "+e.getVertexName2()+" "+ e.getEdgeData().getWeight());
		
		System.out.println();
		G = new Graph<String, data>();
		G.setDirectedGraph();
		G.addVertex("s");
		G.addVertex("v1");
		G.addVertex("v2");
		G.addVertex("v3");
		G.addVertex("v4");
		G.addEdge("s", "v1", new data(2));
		G.addEdge("s", "v3", new data(5));
		G.addEdge("v1", "v3", new data(4));
		G.addEdge("v1", "v2", new data(6));
		G.addEdge("v3", "v2", new data(2));
		G.addEdge("v3", "v4", new data(5));
		G.addEdge("v2", "v4", new data(1));
		System.out.println("Graph 2, s to v4");
		edges=GraphAlgorithms.ShortestPath(G, "s", "v4");
		for(Edge<data> e:edges)
			System.out.println(e.getVertexName1()+" "+e.getVertexName2()+" "+ e.getEdgeData().getWeight());
		System.out.println("Graph 2, v1 to v4");
		edges=GraphAlgorithms.ShortestPath(G, "v1", "v4");
		for(Edge<data> e:edges)
			System.out.println(e.getVertexName1()+" "+e.getVertexName2()+" "+ e.getEdgeData().getWeight());
		System.out.println("Graph 2, s to v2");
		edges=GraphAlgorithms.ShortestPath(G, "s", "v2");
		for(Edge<data> e:edges)
			System.out.println(e.getVertexName1()+" "+e.getVertexName2()+" "+ e.getEdgeData().getWeight());
	}
	
	
}
class data implements IWeight{

	double num;
	
	data(double i){
		num=i;
	}
	
	@Override
	public double getWeight() {
		return num;
	}
		
}