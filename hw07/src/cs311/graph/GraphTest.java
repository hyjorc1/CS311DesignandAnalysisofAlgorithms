package cs311.graph;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cs311.graph.IGraph.DuplicateEdgeException;
import cs311.graph.IGraph.DuplicateVertexException;
import cs311.graph.IGraph.Edge;
import cs311.graph.IGraph.NoSuchEdgeException;
import cs311.graph.IGraph.NoSuchVertexException;
import cs311.graph.IGraph.Vertex;

public class GraphTest {

	Graph<String, Integer> g = new Graph<String, Integer>();
	Graph<String, Integer> G = new Graph<String, Integer>();

	@Before
	public void Test() {
		g.setDirectedGraph();
		g.addVertex("v1", "d1");
		g.addVertex("v2", null);
		g.addVertex("v3");
		g.addEdge("v1", "v2", 1);
		g.addEdge("v2", "v3", null);
		g.addEdge("v3", "v1");
		G.setUndirectedGraph();
		G.addVertex("v1", "d1");
		G.addVertex("v2", null);
		G.addVertex("v3");
		G.addEdge("v1", "v2", 1);
		G.addEdge("v2", "v3", null);
		G.addEdge("v3", "v1");
	}

	@Test(expected = DuplicateVertexException.class)
	public void Test1() {
		g.addVertex("v1", "d1");
	}

	@Test(expected = DuplicateVertexException.class)
	public void Test2() {
		g.addVertex("v1");
	}

	@Test(expected = DuplicateEdgeException.class)
	public void Test3() {
		g.addEdge("v1", "v2");
	}

	@Test(expected = DuplicateEdgeException.class)
	public void Test4() {
		g.addEdge("v1", "v2", 1);
	}

	@Test(expected = DuplicateEdgeException.class)
	public void Test5() {
		G.addEdge("v2", "v1", 1);
	}

	@Test(expected = DuplicateEdgeException.class)
	public void Test6() {
		G.addEdge("v1", "v3");
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test7() {
		G.addEdge("v4", "v3");
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test8() {
		G.addEdge("v3", "v4");
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test9() {
		G.addEdge("v4", "v4");
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test10() {
		g.addEdge("v4", "v3");
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test11() {
		g.addEdge("v3", "v4");
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test12() {
		g.addEdge("v4", "v4");
	}

	@Test
	public void Test13() {
		assertEquals("d1", g.getVertexData("v1"));
	}

	@Test
	public void Test14() {
		assertEquals(null, g.getVertexData("v2"));
	}

	@Test
	public void Test15() {
		assertEquals(null, g.getVertexData("v3"));
	}

	@Test
	public void Test16() {
		assertEquals("d1", G.getVertexData("v1"));
	}

	@Test
	public void Test17() {
		assertEquals(null, G.getVertexData("v2"));
	}

	@Test
	public void Test18() {
		assertEquals(null, G.getVertexData("v3"));
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test19() {
		assertEquals(null, G.getVertexData("v4"));
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test20() {
		assertEquals(null, g.getVertexData("v4"));
	}

	@Test
	public void Test21() {
		g.setVertexData("v1", "d2");
		g.setVertexData("v2", "d2");
		g.setVertexData("v3", "d2");
		assertEquals("d2", g.getVertexData("v1"));
		assertEquals("d2", g.getVertexData("v2"));
		assertEquals("d2", g.getVertexData("v3"));
	}

	@Test
	public void Test22() throws NoSuchVertexException, NoSuchEdgeException {
		int a = g.getEdgeData("v1", "v2");
		assertEquals(1, a);
		Integer b = g.getEdgeData("v2", "v3");
		assertEquals(null, b);
		b = g.getEdgeData("v3", "v1");
		assertEquals(null, b);
	}

	@Test
	public void Test23() throws NoSuchVertexException, NoSuchEdgeException {
		int a = G.getEdgeData("v1", "v2");
		assertEquals(1, a);
		Integer b = G.getEdgeData("v2", "v3");
		assertEquals(null, b);
		b = G.getEdgeData("v3", "v1");
		assertEquals(null, b);

		a = G.getEdgeData("v2", "v1");
		assertEquals(1, a);
		b = G.getEdgeData("v3", "v2");
		assertEquals(null, b);
		b = G.getEdgeData("v1", "v3");
		assertEquals(null, b);
	}

	@Test(expected = NoSuchEdgeException.class)
	public void Test24() throws NoSuchVertexException, NoSuchEdgeException {
		g.getEdgeData("v3", "v2");
	}

	@Test(expected = NoSuchVertexException.class)
	public void Test25() throws NoSuchVertexException, NoSuchEdgeException {
		g.getEdgeData("v4", "v2");
	}

	@Test
	public void Test26() throws NoSuchVertexException, NoSuchEdgeException {
		g.setEdgeData("v1", "v2", 2);
		g.setEdgeData("v2", "v3", 2);
		g.setEdgeData("v3", "v1", 2);
		int a = g.getEdgeData("v1", "v2");
		int b = g.getEdgeData("v2", "v3");
		int c = g.getEdgeData("v3", "v1");
		assertEquals(2, a);
		assertEquals(2, b);
		assertEquals(2, c);
		
		G.setEdgeData("v1", "v2", 2);
		G.setEdgeData("v2", "v3", 2);
		G.setEdgeData("v3", "v1", 2);
		a = G.getEdgeData("v1", "v2");
		b = G.getEdgeData("v2", "v3");
		c = G.getEdgeData("v3", "v1");
		assertEquals(2, a);
		assertEquals(2, b);
		assertEquals(2, c);
		
		G.setEdgeData("v2", "v1", 3);
		G.setEdgeData("v3", "v2", 3);
		G.setEdgeData("v1", "v3", 3);
		a = G.getEdgeData("v1", "v2");
		b = G.getEdgeData("v2", "v3");
		c = G.getEdgeData("v3", "v1");
		assertEquals(3, a);
		assertEquals(3, b);
		assertEquals(3, c);
		
		G.setEdgeData("v1", "v2", null);
		G.setEdgeData("v2", "v3", 4);
		G.setEdgeData("v3", "v1", 4);
		Integer a1 = G.getEdgeData("v2", "v1");
		b = G.getEdgeData("v3", "v2");
		c = G.getEdgeData("v1", "v3");
		assertEquals(null, a1);
		assertEquals(4, b);
		assertEquals(4, c);
		assertEquals(3, G.getEdges().size());
		assertEquals(3, g.getEdges().size());
	}
	
	@Test(expected = NoSuchVertexException.class)
	public void Test27() {
		g.getVertex("v4");
	}
	
	@Test
	public void Test28() {
		assertEquals("d1", g.getVertex("v1").getVertexData());
	}
	
	@Test
	public void Test29() throws NoSuchVertexException, NoSuchEdgeException{
		assertEquals(1, g.getNeighbors("v1").size());
		assertEquals(1, g.getNeighbors("v2").size());
		assertEquals(1, g.getNeighbors("v3").size());
		
		assertEquals(2, G.getNeighbors("v1").size());
		assertEquals(2, G.getNeighbors("v2").size());
		assertEquals(2, G.getNeighbors("v3").size());
		
		assertEquals(new Vertex<String>("v2",null), g.getNeighbors("v1").get(0));
		assertEquals(new Vertex<String>("v3",null), g.getNeighbors("v2").get(0));
		assertEquals(new Vertex<String>("v1",null), g.getNeighbors("v3").get(0));
		
		assertEquals(new Vertex<String>("v2",null), G.getNeighbors("v1").get(0));
		assertEquals(new Vertex<String>("v3",null), G.getNeighbors("v1").get(1));
		
		assertEquals(new Vertex<String>("v1",null), G.getNeighbors("v2").get(0));
		assertEquals(new Vertex<String>("v3",null), G.getNeighbors("v2").get(1));
		
		g.addVertex("v4");
		g.addEdge("v4", "v1", 41);
		assertEquals(1, g.getNeighbors("v1").size());
		
		g.addEdge("v1", "v4", 14);
		assertEquals(2, g.getNeighbors("v1").size());
		
		int a = g.getEdgeData("v4", "v1");
		int b = g.getEdgeData("v1", "v4");
		assertEquals(41, a);
		assertEquals(14, b);
		
		G.addVertex("v4");
		G.addEdge("v4", "v1");
		assertEquals(3, G.getNeighbors("v1").size());
	}
}
