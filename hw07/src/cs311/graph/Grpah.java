package cs311.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Grpah<V, E> implements IGraph<V, E> {

	private HashMap<String, Vertex<V>> vmap;

	private HashMap<String, HashSet<String>> nmap;

	private HashMap<Integer, Edge<E>> emap;

	private boolean isDirectedGraph;

	@Override
	public void setDirectedGraph() {
		// TODO Auto-generated method stub
		isDirectedGraph = true;
	}

	@Override
	public void setUndirectedGraph() {
		// TODO Auto-generated method stub
		isDirectedGraph = false;
	}

	@Override
	public boolean isDirectedGraph() {
		// TODO Auto-generated method stub
		return isDirectedGraph;
	}

	@Override
	public void addVertex(String vertexName) throws DuplicateVertexException {
		// TODO Auto-generated method stub
		if (hasVertex(vertexName))
			throw new DuplicateVertexException();
		vmap.put(vertexName, new Vertex<V>(vertexName, null));
	}

	@Override
	public void addVertex(String vertexName, V vertexData) throws DuplicateVertexException {
		// TODO Auto-generated method stub
		if (hasVertex(vertexName))
			throw new DuplicateVertexException();
		vmap.put(vertexName, new Vertex<V>(vertexName, vertexData));
	}

	@Override
	public void addEdge(String vertex1, String vertex2) throws DuplicateEdgeException, NoSuchVertexException {
		// TODO Auto-generated method stub
		if (hasEdge(vertex1, vertex2))
			throw new DuplicateEdgeException();
		nmap.get(vertex1).add(vertex2);
		if (!isDirectedGraph)
			nmap.get(vertex2).add(vertex1);
		emap.put(new Edge<E>(vertex1, vertex2, null).hashCode(), new Edge<E>(vertex1, vertex2, null));
	}

	@Override
	public void addEdge(String vertex1, String vertex2, E edgeData)
			throws DuplicateEdgeException, NoSuchVertexException {
		// TODO Auto-generated method stub
		if (hasEdge(vertex1, vertex2))
			throw new DuplicateEdgeException();
		nmap.get(vertex1).add(vertex2);
		if (!isDirectedGraph)
			nmap.get(vertex2).add(vertex1);
		emap.put(new Edge<E>(vertex1, vertex2, null).hashCode(), new Edge<E>(vertex1, vertex2, null));
	}

	@Override
	public V getVertexData(String vertexName) throws NoSuchVertexException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertexName))
			throw new NoSuchVertexException();
		return vmap.get(vertexName).getVertexData();
	}

	@Override
	public void setVertexData(String vertexName, V vertexData) throws NoSuchVertexException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertexName))
			throw new NoSuchVertexException();
		vmap.replace(vertexName, new Vertex<V>(vertexName, vertexData));
	}

	@Override
	public E getEdgeData(String vertex1, String vertex2) throws NoSuchVertexException, NoSuchEdgeException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertex1) || !hasVertex(vertex2))
			throw new NoSuchVertexException();
		if (!hasEdge(vertex1, vertex2))
			throw new NoSuchEdgeException();
		return getEdge(vertex1, vertex2).getEdgeData();
	}

	@Override
	public void setEdgeData(String vertex1, String vertex2, E edgeData)
			throws NoSuchVertexException, NoSuchEdgeException {
		// TODO Auto-generated method stub
		if (!hasVertex(vertex1) || !hasVertex(vertex2))
			throw new NoSuchVertexException();
		if (!hasEdge(vertex1, vertex2))
			throw new NoSuchEdgeException();
		//////////
	}

	@Override
	public Vertex<V> getVertex(String VertexName) {
		// TODO Auto-generated method stub
		return vmap.get(VertexName);
	}

	@Override
	public Edge<E> getEdge(String vertexName1, String vertexName2) {
		// TODO Auto-generated method stub
		if (emap.containsKey(new Edge<E>(vertexName1, vertexName2, null).hashCode()))
			return emap.get(new Edge<E>(vertexName1, vertexName2, null).hashCode());
		return emap.get(new Edge<E>(vertexName2, vertexName1, null).hashCode());
	}

	@Override
	public List<Vertex<V>> getVertices() {
		// TODO Auto-generated method stub
		List<Vertex<V>> vlist = new ArrayList<Vertex<V>>();
		for (String e : vmap.keySet())
			vlist.add(vmap.get(e));
		return vlist;
	}

	@Override
	public List<Edge<E>> getEdges() {
		// TODO Auto-generated method stub
		List<Edge<E>> elist = new ArrayList<Edge<E>>();
		for (Integer e : emap.keySet())
			elist.add(emap.get(e));
		return elist;
	}

	@Override
	public List<Vertex<V>> getNeighbors(String vertex) {
		// TODO Auto-generated method stub
		List<Vertex<V>> nlist = new ArrayList<Vertex<V>>();
		for (String e : nmap.get(vertex))
			nlist.add(vmap.get(e));
		return nlist;
	}

	public boolean hasVertex(String s) {
		return vmap.containsKey(s);
	}

	public boolean hasEdge(String s1, String s2) {
		if (emap.containsKey(new Edge<E>(s1, s2, null).hashCode()))
			return true;
		if (!isDirectedGraph && emap.containsKey(new Edge<E>(s2, s1, null).hashCode()))
			return true;
		return false;
	}
}
