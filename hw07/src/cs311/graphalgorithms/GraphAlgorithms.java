
package cs311.graphalgorithms;

import cs311.graph.IGraph;
import cs311.graph.IGraph.Edge;
import cs311.graph.IGraph.Vertex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

public class GraphAlgorithms {

	public static <V, E> List<Vertex<V>> TopologicalSort(IGraph<V, E> g) {
		HashMap<String, String> statemap = new HashMap<String, String>();
		for (Vertex<V> e : g.getVertices())
			statemap.put(e.getVertexName(), "Undiscovered");
		LinkedStack<Vertex<V>> stack = new LinkedStack<Vertex<V>>();
		for (Vertex<V> e : g.getVertices())
			if (statemap.get(e.getVertexName()).equals("Undiscovered"))
				DFS(e, g, statemap, stack);
		List<Vertex<V>> vlist = new ArrayList<Vertex<V>>();
		while (!stack.isEmpty())
			vlist.add(stack.pop());
		return vlist;
	}

	public static <V, E> List<List<Vertex<V>>> AllTopologicalSort(IGraph<V, E> g) {
		List<List<Vertex<V>>> vlistlist = new ArrayList<List<Vertex<V>>>();
		List<Vertex<V>> vlist = new ArrayList<Vertex<V>>();
		HashMap<String, String> statemap = new HashMap<String, String>();
		HashMap<String, Integer> indegreemap = new HashMap<String, Integer>();
		for (Vertex<V> e : g.getVertices()) {
			statemap.put(e.getVertexName(), "Undiscovered");
			for (Vertex<V> ele : g.getNeighbors(e.getVertexName()))
				if (!indegreemap.containsKey(ele.getVertexName()))
					indegreemap.put(ele.getVertexName(), 1);
				else
					indegreemap.replace(ele.getVertexName(), indegreemap.get(ele.getVertexName()) + 1);
		}
		AllTopologicalSortRec(g, vlistlist, vlist, statemap, indegreemap);
		return vlistlist;
	}

	public static <V, E extends IWeight> IGraph<V, E> Kruscal(IGraph<V, E> g) {

		Heap<E> pq = new Heap<E>();
		for(Edge<E> e:g.getEdges())
			pq.add(e);
		List<List<String>> comp = new ArrayList<List<String>>();
		IGraph<V, E> G;
		for()
		
		return null;
	}

	public static <V, E> void AllTopologicalSortRec(IGraph<V, E> g, List<List<Vertex<V>>> vlistlist,
			List<Vertex<V>> vlist, HashMap<String, String> statemap, HashMap<String, Integer> indegreemap) {
		for (Vertex<V> e : g.getVertices()) {
			if ((indegreemap.get(e.getVertexName()) == null || indegreemap.get(e.getVertexName()) == 0)
					&& statemap.get(e.getVertexName()).equals("Undiscovered")) {
				statemap.replace(e.getVertexName(), "discovered");
				vlist.add(e);
				if (vlist.size() == g.getVertices().size()) {
					List<Vertex<V>> list = new ArrayList<Vertex<V>>();
					for (Vertex<V> ele : vlist)
						list.add(ele);
					vlistlist.add(list);
				}
				for (Vertex<V> ele : g.getNeighbors(e.getVertexName()))
					indegreemap.replace(ele.getVertexName(), indegreemap.get(ele.getVertexName()) - 1);
				AllTopologicalSortRec(g, vlistlist, vlist, statemap, indegreemap);
				statemap.replace(e.getVertexName(), "Undiscovered");
				vlist.remove(vlist.size() - 1);
				for (Vertex<V> ele : g.getNeighbors(e.getVertexName()))
					indegreemap.replace(ele.getVertexName(), indegreemap.get(ele.getVertexName()) + 1);
			}
		}
	}

	public static <V, E> void DFS(Vertex<V> v, IGraph<V, E> g, HashMap<String, String> statemap,
			LinkedStack<Vertex<V>> stack) {
		statemap.replace(v.getVertexName(), "discovered");
		for (Vertex<V> e : g.getNeighbors(v.getVertexName()))
			if (statemap.get(e.getVertexName()).equals("Undiscovered"))
				DFS(e, g, statemap, stack);
		stack.push(v);
	}

	static class LinkedStack<E> {
		private class SNode {
			public E data;
			public SNode link;
		}

		private SNode top; // refers to the top node of the stack.
		private int numItems; // number of elements in the stack.

		public LinkedStack() {
			top = null;
			numItems = 0;
		}

		public int size() {
			return numItems;
		}

		public boolean isEmpty() {
			return numItems == 0;
		}

		public void push(E element) {
			SNode toAdd = new SNode();
			toAdd.data = element;
			toAdd.link = top;
			top = toAdd;
			numItems++;
		}

		public E pop() {
			if (top == null)
				throw new NoSuchElementException();
			E returnVal = top.data;
			top = top.link;
			if (numItems <= 0)
				throw new RuntimeException("An incorrect number of elements");
			numItems--;
			return returnVal;
		}

		public E peek() {
			if (top == null)
				throw new NoSuchElementException();
			return top.data;
		}
	}

	static class Heap<E extends IWeight> {
		private static final int INIT_CAP = 10;
		private ArrayList<Edge<E>> list;

		public Heap() {
			list = new ArrayList<Edge<E>>(INIT_CAP);
		}

		public int size() {
			return list.size();
		}

		public boolean isEmpty() {
			return list.isEmpty();
		}

		public String toString() {
			return list.toString();
		}

		public void add(Edge<E> element) {
			if (element == null)
				throw new NullPointerException("add");
			list.add(element); // append it to the end of the list
			percolateUp(); // move it up to the proper place
		}

		// move the last element up to the proper place.
		private void percolateUp() {
			int child = list.size() - 1; // last element in the list
			int parent;
			while (child > 0) {
				parent = (child - 1) / 2; // use the (j-1)/2 formula
				if (list.get(child).getEdgeData().getWeight() - list.get(parent).getEdgeData().getWeight() >= 0)
					break;
				swap(parent, child);
				child = parent;
			}
		}

		private void swap(int parent, int child) {
			Edge<E> tmp = list.get(parent);
			list.set(parent, list.get(child));
			list.set(child, tmp);
		}

		public Edge<E> getMin() {
			if (list.isEmpty())
				throw new NoSuchElementException();
			return list.get(0);
		}

		public Edge<E> removeMin() {
			if (list.isEmpty())
				throw new NoSuchElementException();
			Edge<E> minElem = list.get(0); // get the min element at the root
			list.set(0, list.get(list.size() - 1)); // copy the last element to
													// the
													// root
			list.remove(list.size() - 1); // remove the last element from the
											// list
			if (!list.isEmpty())
				percolateDown(0); // move the element at the root down to the
									// proper
									// place
			return minElem;
		}

		// Move the element at index start down to the proper place.
		private void percolateDown(int start) {
			if (start < 0 || start >= list.size())
				throw new RuntimeException("start < 0 or >= n");
			int parent = start;
			int child = 2 * parent + 1; // use the 2*i+1 formula
			while (child < list.size()) {
				if (child + 1 < list.size() && list.get(child).getEdgeData().getWeight()
						- list.get(child + 1).getEdgeData().getWeight() > 0)
					child++; // select the smaller child
				if (list.get(child).getEdgeData().getWeight() - list.get(parent).getEdgeData().getWeight() >= 0)
					break; // reach the proper place
				swap(parent, child);
				parent = child;
				child = 2 * parent + 1;
			}
		}
	} // Heap
}
