package cs311.graphalgorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cs311.graph.Graph;
import cs311.graph.IGraph.Vertex;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Graph<String, Integer> g = new Graph<String, Integer>();
		g.setDirectedGraph();
		g.addVertex("v0");
		g.addVertex("v1");
		g.addVertex("v2");
		g.addVertex("v3");
		g.addVertex("v4");
		g.addVertex("v5");
		g.addEdge("v5", "v0");
		g.addEdge("v5", "v2");
		g.addEdge("v2", "v3");
		g.addEdge("v3", "v1");
		g.addEdge("v4", "v0");
		g.addEdge("v4", "v1");
		List<Vertex<String>> vlist = new ArrayList<Vertex<String>>();
		vlist = GraphAlgorithms.TopologicalSort(g);
		System.out.println("TOPOLOGICALSORT");
		for (Vertex<String> e : vlist)
			System.out.print(e.getVertexName() + " ");
		System.out.println();
		System.out.println();
		System.out.println("ALL TOPOLOGICALSORT");
		List<List<Vertex<String>>> vlistlist = GraphAlgorithms.AllTopologicalSort(g);
		for (List<Vertex<String>> e : vlistlist) {
			for (Vertex<String> ele : e)
				System.out.print(ele.getVertexName() + " ");
			System.out.println();
		}
		
		Graph<String, testdata> G = new Graph<String, testdata>();
		g.addVertex("v0");
		g.addVertex("v1");
		g.addVertex("v2");
		g.addVertex("v3");
		g.addVertex("v4");
		g.addVertex("v5");
		G.addEdge(vertex1, vertex2);
	}
	
	
}
class testdata implements IWeight{

	double num;
	
	testdata(double i){
		num=i;
	}
	
	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return num;
	}
		
}