package hw03;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class QuickSort implements SortAnalysis<Integer> {

	public static void main(String[] args) {
		// Part 3 a)
		// The worst case usually happens for the quick sort is that the pivot
		// is the smallest or largest element in the list. It takes big O of n
		// square. For my quick sort method, I always choose the first element
		// as my pivot. Therefore,a sorted (or reverse-sorted) array will be my
		// worst case. Since my quick sort method is a recursive version, a
		// completely sorted array will cause stack overflow in eclipse. Then I
		// provide a almost sorted array to be my worst case for analysis.
		
		// b)
		long runtime = 0;
		int n = 1;
		QuickSort sort = new QuickSort();
		while (runtime <= 1000) {
			n *= 2;
			runtime = sort.analyzeSort(sort.getList(n, "WC"));
		}
		System.out.println("Part 3 b)");
		System.out.println("ArrayList Size: " + n + ", Running Time: " + runtime);
		HashMap<Integer, Long> map = getExactData(n, "WC");
		Map.Entry<Integer, Long> entry = map.entrySet().iterator().next();
		System.out.println(
				"Exact ArrayList Size Over 1000 ms: " + entry.getKey() + ", Running Time: " + entry.getValue());

		// c)
		System.out.println("Part 3 c)");
		for (int i = 0; i < entry.getKey(); i += entry.getKey() / 50) {
			// System.out.println(sort.analyzeSort(sort.getList(i, "WC")));
			System.out.println("ArrayList Size: " + i + ", Running Time: " + sort.analyzeSort(sort.getList(i, "WC")));
		}

		// f_b)
		long runtime_f = 0;
		int n_f = 1;
		QuickSort sort_f = new QuickSort();
		while (runtime_f <= 1000) {
			n_f *= 2;
			runtime_f = sort_f.analyzeSort(sort_f.getList(n_f, "rand"));
		}
		System.out.println("Part 3 f_b)");
		System.out.println("ArrayList Size: " + n_f + ", Running Time: " + runtime_f);
		HashMap<Integer, Long> map_f = getExactData(n_f, "rand");
		Map.Entry<Integer, Long> entry_f = map_f.entrySet().iterator().next();
		System.out.println(
				"Exact ArrayList Size Over 1000 ms: " + entry_f.getKey() + ", Running Time: " + entry_f.getValue());

		// f_c)
		System.out.println("Part 3 f_c)");
		for (int i = 0; i < entry_f.getKey(); i += entry_f.getKey() / 50) {
			// System.out.println(sort_f.analyzeSort(sort_f.getList(i,
			// "rand")));
			System.out.println(
					"ArrayList Size: " + i + ", Running Time: " + sort_f.analyzeSort(sort_f.getList(i, "rand")));
		}
	}

	/**
	 * return A HashMap containing the exact entry with the arraylist size and
	 * running time exact over 1000 ms
	 * 
	 * @param n
	 *            arraylist size
	 * @param s
	 *            If s is "WC", this method runs under a worse case, otherwise
	 *            it runs under a random data.
	 * @return A HashMap containing the exact entry with the arraylist size and
	 *         running time exact over 1000 ms
	 */
	public static HashMap<Integer, Long> getExactData(int n, String s) {
		int start = 1;
		int end = n;
		int mid;
		long runtime;
		QuickSort sort = new QuickSort();
		HashMap<Long, Integer> data = new HashMap<Long, Integer>();
		while (start <= end) {
			mid = (start + end) / 2;
			runtime = sort.analyzeSort(sort.getList(mid, s));
			if (runtime < 1000)
				start = mid + 1;
			else {
				end = mid - 1;
				data.put(runtime, mid);
			}
			// System.out.println("ArrayList Size: " + mid + ", Running Time: "
			// + runtime);
		}
		long min = Long.MAX_VALUE;
		for (Long e : data.keySet())
			if (e <= min)
				min = e;
		HashMap<Integer, Long> result = new HashMap<Integer, Long>();
		result.put(data.get(min), min);
		return result;
	}

	@Override
	public long analyzeSort(ArrayList<Integer> list) {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis();
		quickSortRec(list, 0, list.size() - 1);
		long end = System.currentTimeMillis();
		return end - start;
	}

	/**
	 * Recursive method of quick sort to sort left and right parts recursively.
	 * 
	 * @param arr
	 *            the arraylist needed to sort
	 * @param lo
	 *            index of the left side
	 * @param hi
	 *            index of the right side
	 */
	private static void quickSortRec(ArrayList<Integer> arr, int lo, int hi) {
		if (lo < hi) {
			Integer mid = partition(arr, lo, hi);
			quickSortRec(arr, lo, mid);
			quickSortRec(arr, mid + 1, hi);
		}
	}

	/**
	 * Return a partition index based on Hoare partiton scheme
	 * 
	 * @param arr
	 *            the arraylist needed to sort
	 * @param lo
	 *            index of the top whose element should less than the pivot
	 * @param hi
	 *            index of the bot whose element should greater than the pivot
	 * @return partition index
	 */
	private static int partition(ArrayList<Integer> arr, int lo, int hi) {
		Integer pivot = arr.get(lo);
		int top = lo;
		int bot = hi;
		while (true) {
			while (arr.get(top) < pivot)
				top++;
			while (arr.get(bot) > pivot)
				bot--;
			if (top < bot) {
				Integer tmp = arr.get(top);
				arr.set(top++, arr.get(bot));
				arr.set(bot--, tmp);
			} else
				break;
		}
		return bot;
	}

	/**
	 * Return a list with worst case or random data which depends on String s.
	 * 
	 * @param n
	 *            arraylist size
	 * @param s
	 *            If s is "WC", this method gives a worse case, otherwise it
	 *            gives a random data.
	 * @return a list with worst case or random data which depends on String s
	 */
	public ArrayList<Integer> getList(int n, String s) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		Random rand = new Random();
		if (s == "WC") {
			for (int i = 0; i < n; i++) {
				if (i % 1000 == 0)
					arr.add(i / 3);
				arr.add(i);
			}
			return arr;
		}
		for (int i = n; i > 0; i--)
			arr.add(rand.nextInt(n));
		return arr;
	}
}
