package hw03;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MatrixOperations implements MatrixAnalysis {

	public static void main(String[] args) {

		// // Part 1 a)
		long runtime = 0;
		int n = 1;
		MatrixOperations m = new MatrixOperations();
		while (runtime <= 1000) {
			n *= 2;
			runtime = m.runTime(n, "multi");
		}
		System.out.println("Part 1 a)");
		System.out.println("Matrix Size: " + n + ", Running Time: " + runtime);
		HashMap<Integer, Long> map = getExactData(n, "multi");
		Map.Entry<Integer, Long> entry = map.entrySet().iterator().next();
		System.out.println("Exact Matrix Size Over 1000 ms: " + entry.getKey() + ", Running Time: " + entry.getValue());

		// b)
		System.out.println("Part 1 b)");
		for (int i = 0; i < entry.getKey(); i += entry.getKey() / 20) {
			// System.out.println(m.runTime(i, "multi"));
			System.out.println("Matrix Size: " + i + ", Running Time: " + m.runTime(i, "multi"));
		}

		// Part 4 a)

		// MatrixOperations m = new MatrixOperations();
		// double[][] m1 = {{3,2,3},{4,5,6},{7,8,9}};
		// double[][] m2 = new double[3][3];
		// m.inverse(m1,m2);
		// m.printMatrix(m2);

		long runtime_4 = 0;
		int n_4 = 1;
		MatrixOperations m_4 = new MatrixOperations();
		while (runtime_4 <= 1000) {
			n_4 *= 2;
			runtime_4 = m_4.runTime(n_4, "inverse");
		}
		System.out.println("Part 4 a)");
		System.out.println("Matrix Size: " + n_4 + ", Running Time: " + runtime_4);
		HashMap<Integer, Long> map_4 = getExactData(n_4, "inverse");
		Map.Entry<Integer, Long> entry_4 = map_4.entrySet().iterator().next();
		System.out.println(
				"Exact Matrix Size Over 1000 ms: " + entry_4.getKey() + ", Running Time: " + entry_4.getValue());

		// b)
		System.out.println("Part 4 b)");
		for (int i = 0; i < entry_4.getKey(); i += entry_4.getKey() / 20) {
			// System.out.println(m_4.runTime(i, "inverse"));
			System.out.println("Matrix Size: " + i + ", Running Time: " + m_4.runTime(i, "inverse"));
		}

	}

	/**
	 * return A HashMap containing the exact entry with the matrix size and
	 * running time exact over 1000 ms
	 * 
	 * @param n
	 *            Size of the square matrix
	 * @param s
	 *            If s is "multi", multiplication operation will be applied. If
	 *            s is "inverse", inverse operation will be applied
	 * @return A HashMap containing the exact entry with the matrix size and
	 *         running time exact over 1000 ms
	 */
	public static HashMap<Integer, Long> getExactData(int n, String s) {
		int start = 1;
		int end = n;
		int mid;
		long runtime;
		MatrixOperations m = new MatrixOperations();
		HashMap<Long, Integer> data = new HashMap<Long, Integer>();
		while (start <= end) {
			mid = (start + end) / 2;
			runtime = m.runTime(mid, s);
			if (runtime < 1000)
				start = mid + 1;
			else {
				end = mid - 1;
				data.put(runtime, mid);
			}
			// System.out.println("Matrix Size: " + mid + ", Running Time: " +
			// runtime);
		}
		long min = Long.MAX_VALUE;
		for (Long e : data.keySet())
			if (e <= min)
				min = e;
		HashMap<Integer, Long> result = new HashMap<Integer, Long>();
		result.put(data.get(min), min);
		return result;
	}

	/**
	 * return running time for the square matrix multiplication
	 * 
	 * @param n
	 *            Size of the square matrix
	 * @param s
	 *            If s is "multi", multiplication operation will be applied. If
	 *            s is "inverse", inverse operation will be applied.
	 * @return running time for the square matrix multiplication
	 */
	public long runTime(int n, String s) {
		MatrixOperations m = new MatrixOperations();
		double[][] m3 = new double[n][n];
		if (s == "multi")
			return m.analyzeMultiply(m.randomMatrix(n), m.randomMatrix(n), m3);
		return m.analyzeInverse(m.randomMatrix(n), m3);
	}

	/**
	 * Used to create a random square matrix
	 * 
	 * @param n
	 *            Size of the square matrix
	 * @return double 2-D array
	 */
	public double[][] randomMatrix(int n) {
		Random rand = new Random();
		double[][] m = new double[n][n];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				m[i][j] = rand.nextDouble();
		return m;
	}

	@Override
	public long analyzeMultiply(double[][] m1, double[][] m2, double[][] m3) {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis();
		multiply(m1, m2, m3);
		long end = System.currentTimeMillis();
		return end - start;
	}

	/**
	 * Matrix multiplication
	 * 
	 * @param m1
	 *            The first square matrix to multiply
	 * @param m2
	 *            The second square matrix to multiply
	 * @param m3
	 *            The result is placed in the square matrix m3 = m1 * m2
	 */
	public void multiply(double[][] m1, double[][] m2, double[][] m3) {
		int n = m1.length;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++) {
				double sum = 0;
				for (int k = 0; k < n; k++)
					sum += m1[i][k] * m2[k][j];
				m3[i][j] = sum;
			}
	}

	@Override
	public long analyzeInverse(double[][] m1, double[][] m2) {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis();
		inverse(m1, m2);
		long end = System.currentTimeMillis();
		return end - start;
	}

	/**
	 * Matrix inverse
	 * 
	 * @param m
	 *            The square matrix to take the inverse of
	 * @param m2
	 *            The resultant inverse
	 */
	public void inverse(double[][] m, double[][] m2) {
		int n = m.length;
		double[][] m1 = new double[n][n];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++) {
				m1[i][j] = m[i][j];
				m2[i][j] = 0;
				if (i == j)
					m2[i][j] = 1;
			}
		for (int i = 0; i < n; i++) {
			double tmp = m1[i][i];
			for (int j = 0; j < n; j++) {
				m1[i][j] = m1[i][j] / tmp;
				m2[i][j] = m2[i][j] / tmp;
			}
			for (int k = i + 1; k < n; k++)
				RowOperation(i, m1, k, m2, m1[k][i]);
		}
		for (int i = 1; i < n; i++)
			for (int j = 0; j < i; j++)
				RowOperation(i, m1, j, m2, m1[j][i]);
	}

	/**
	 * Row operation based on Gauss-Jordan Elimination method. Row b of m2 minus
	 * the constant c times Row a of m1. Row b of m1 minus the constant c times
	 * Row a of m1.
	 * 
	 * @param a
	 *            row #
	 * @param m1
	 *            a square matrix
	 * @param b
	 *            row #
	 * @param m2
	 *            a square matrix
	 * @param c
	 *            multiplication coefficient
	 */
	public void RowOperation(int a, double[][] m1, int b, double[][] m2, double c) {
		for (int i = 0; i < m1.length; i++) {
			m1[b][i] = m1[b][i] - c * m1[a][i];
			m2[b][i] = m2[b][i] - c * m2[a][i];
		}
	}

	/**
	 * print matrix
	 * 
	 * @param m
	 *            the square matrix needed to print
	 */
	public void printMatrix(double[][] m) {
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[0].length; j++)
				System.out.print(m[i][j] + " ");
			System.out.println();
		}
		System.out.println();
	}
}
