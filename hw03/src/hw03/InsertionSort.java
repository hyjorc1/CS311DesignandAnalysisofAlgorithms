package hw03;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class InsertionSort implements SortAnalysis<Integer> {

	public static void main(String[] args) {

		// Part 2 a)
		// The worst case is an reversely sorted array. It takes big O of n
		// square.

		// b)
		long runtime = 0;
		int n = 1;
		InsertionSort sort = new InsertionSort();
		while (runtime <= 1000) {
			n *= 2;
			runtime = sort.analyzeSort(sort.getList(n, "WC"));
		}
		System.out.println("Part 2 b)");
		System.out.println("ArrayList Size: " + n + ", Running Time: " + runtime);
		HashMap<Integer, Long> map = getExactData(n, "WC");
		Map.Entry<Integer, Long> entry = map.entrySet().iterator().next();
		System.out.println(
				"Exact ArrayList Size Over 1000 ms: " + entry.getKey() + ", Running Time: " + entry.getValue());

		// c)
		System.out.println("Part 2 c)");
		for (int i = 0; i < entry.getKey(); i += entry.getKey() / 50) {
			// System.out.println(sort.analyzeSort(sort.getList(i, "WC")));
			System.out.println("ArrayList Size: " + i + ", Running Time: " + sort.analyzeSort(sort.getList(i, "WC")));
		}

		// f_b)
		long runtime_f = 0;
		int n_f = 1;
		InsertionSort sort_f = new InsertionSort();
		while (runtime_f <= 1000) {
			n_f *= 2;
			runtime_f = sort_f.analyzeSort(sort_f.getList(n_f, "rand"));
		}
		System.out.println("f_b)");
		System.out.println("ArrayList Size: " + n_f + ", Running Time: " + runtime_f);
		HashMap<Integer, Long> map_f = getExactData(n_f, "rand");
		Map.Entry<Integer, Long> entry_f = map_f.entrySet().iterator().next();
		System.out.println(
				"Exact ArrayList Size Over 1000 ms: " + entry_f.getKey() + ", Running Time: " + entry_f.getValue());

		// f_c)
		System.out.println("f_c)");
		for (int i = 0; i < entry_f.getKey(); i += entry_f.getKey() / 50) {
			// System.out.println(sort_f.analyzeSort(sort_f.getList(i,
			// "rand")));
			System.out.println(
					"ArrayList Size: " + i + ", Running Time: " + sort_f.analyzeSort(sort_f.getList(i, "rand")));
		}

	}

	/**
	 * return A HashMap containing the exact entry with the arraylist size and
	 * running time exact over 1000 ms
	 * 
	 * @param n
	 *            arraylist size
	 * @param s
	 *            If s is "WC", this method runs under a worse case, otherwise
	 *            it runs under a random data.
	 * @return A HashMap containing the exact entry with the arraylist size and
	 *         running time exact over 1000 ms
	 */
	public static HashMap<Integer, Long> getExactData(int n, String s) {
		int start = 1;
		int end = n;
		int mid;
		long runtime;
		InsertionSort sort = new InsertionSort();
		HashMap<Long, Integer> data = new HashMap<Long, Integer>();
		while (start <= end) {
			mid = (start + end) / 2;
			runtime = sort.analyzeSort(sort.getList(mid, s));
			if (runtime < 1000)
				start = mid + 1;
			else {
				end = mid - 1;
				data.put(runtime, mid);
			}
			// System.out.println("ArrayList Size: " + mid + ", Running Time: "
			// + runtime);
		}
		long min = Long.MAX_VALUE;
		for (Long e : data.keySet())
			if (e <= min)
				min = e;
		HashMap<Integer, Long> result = new HashMap<Integer, Long>();
		result.put(data.get(min), min);
		return result;
	}

	/**
	 * Return a list with worst case or random data which depends on String s.
	 * 
	 * @param n
	 *            arraylist size
	 * @param s
	 *            If s is "WC", this method gives a worse case, otherwise it
	 *            gives a random data.
	 * @return a list with worst case or random data which depends on String s
	 */
	public ArrayList<Integer> getList(int n, String s) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		if (s == "WC") {
			for (int i = n; i > 0; i--)
				arr.add(i);
			return arr;
		}
		Random rand = new Random();
		for (int i = n; i > 0; i--)
			arr.add(rand.nextInt(n));
		return arr;
	}

	@Override
	public long analyzeSort(ArrayList<Integer> list) {
		// TODO Auto-generated method stub
		long start = System.currentTimeMillis();
		for (int i = 1; i < list.size(); i++) {
			Integer x = list.get(i);
			int j = i - 1;
			while (j >= 0 && x < list.get(j)) {
				list.set(j + 1, list.get(j));
				j--;
			}
			list.set(j + 1, x);
		}
		long end = System.currentTimeMillis();
		return end - start;
	}
}
